#include "convert.h"
#include <QStringList>
#include <QDebug>
#include <QString>

convert::convert()
{
}

QString convert::asciitoHex(QString toConvert)
{
    QString toReturn="";
    QString toAppend="";
    for(int i=0;i<toConvert.size();i++){
        toAppend=QString::number(toConvert.at(i).unicode(),16).toUpper();
        if(toAppend.size()>2){
            toReturn.chop(toAppend.size()-2);
        }else if(toAppend.size()<2){
            toAppend.prepend('0');
        }
        toReturn.append(toAppend+"|");
    }
    toReturn.chop(1);
    return toReturn;
}

QString convert::asciitoHex(QString toConvert, QString prepend, QString endOfAll)
{
    QString toReturn="";
    QString toAppend="";
    for(int i=0;i<toConvert.size();i++){
        toAppend=QString::number(toConvert.at(i).unicode(),16).toUpper();
        if(toAppend.size()>2){
            toReturn.chop(toAppend.size()-2);
        }else if(toAppend.size()<2){
            toAppend.prepend('0');
        }
        toReturn.append(prepend+toAppend+endOfAll);
    }
    toReturn.chop(1);
    return toReturn.append(endOfAll);
}

QString convert::decimaltoHex(bool * ok,QString toConvert)
{
    QString toReturn="";
    QString toAppend="";
    *ok=false;
    foreach (QString decimal, toConvert.split("|",QString::SkipEmptyParts)){
        toAppend=QString::number(decimal.toInt(ok,10),16).toUpper();
        if(toAppend.size()<2)
            toAppend.prepend('0');
        if(*ok==false || decimal.toInt(NULL,10)>255 || decimal.toInt(NULL,10)<0){
            *ok=false;
            toReturn=toConvert;
            break;
        }
        toReturn.append(toAppend+"|");
    }
    toReturn.chop(1);
    return toReturn;
}

QString convert::bintoHex(bool *ok, QString toConvert)
{
    QString toReturn="";
    QString toAppend="";
    *ok=false;
    foreach (QString bin, toConvert.split("|",QString::SkipEmptyParts)){
        toAppend=QString::number(bin.toInt(ok,2),16).toUpper();
        if(toAppend.size()<2)
            toAppend.prepend('0');
        if(*ok==false || bin.toInt(NULL,2)>255 || bin.toInt(NULL,2)<0){
            *ok=false;
            toReturn=toConvert;
            break;
        }
        toReturn.append(toAppend+"|");
    }
    toReturn.chop(1);
    return toReturn;
}

QString convert::hextoAscii(bool * ok,QString toConvert)
{
    QString toReturn="";
    *ok=false;
    foreach (QString hex, toConvert.split("|",QString::SkipEmptyParts)){
        toReturn.append(QChar(hex.toInt(ok,16)));
        if(*ok==false){
            toReturn=toConvert;
            break;
        }
    }
    return toReturn;
}

QString convert::decimaltoAscii(bool * ok,QString toConvert)
{
    QString toReturn="";
    *ok=false;
    foreach (QString decimal, toConvert.split("|",QString::SkipEmptyParts)){
        toReturn.append(QChar(decimal.toInt(ok,10)));
        if(*ok==false || decimal.toInt(NULL,10)>255 || decimal.toInt(NULL,10)<0){
            *ok=false;
            toReturn=toConvert;
            break;
        }
    }
    return toReturn;
}

QString convert::bintoAscii(bool *ok, QString toConvert)
{
    QString toReturn="";
    *ok=false;
    foreach (QString bin, toConvert.split("|",QString::SkipEmptyParts)){
        toReturn.append(QChar(bin.toInt(ok,2)));
        if(*ok==false || bin.toInt(NULL,2)>255 || bin.toInt(NULL,2)<0){
            *ok=false;
            toReturn=toConvert;
            break;
        }
    }
    return toReturn;
}

QString convert::hextoDecimal(bool * ok,QString toConvert)
{
    QString toReturn="";
    *ok=false;
    foreach (QString hex, toConvert.split("|",QString::SkipEmptyParts)){
        toReturn.append(QString::number(hex.toInt(ok,16),10).append("|"));
        if(*ok==false){
            toReturn=toConvert;
            break;
        }
    }
    toReturn.chop(1);
    return toReturn;
}

QString convert::asciitoDecimal(QString toConvert)
{
    QString toReturn="";
    for(int i=0;i<toConvert.size();i++){
       // QChar(toConvert.at(i))
            toReturn.append(QString::number(toConvert.at(i).unicode(),10).append("|"));
    }
    toReturn.chop(1);
    return toReturn;
}

QString convert::asciitoDecimal(QString toConvert, QString prepend, QString endOfAll)
{
    QString toReturn="";
    for(int i=0;i<toConvert.size();i++){
            toReturn.append(QString::number(toConvert.at(i).unicode(),10).append(endOfAll));
            toReturn.prepend(prepend);
    }
    toReturn.chop(1);
    return toReturn.append(endOfAll);
}

QString convert::bintoDecimal(bool *ok, QString toConvert)
{
    QString toReturn="";
    *ok=false;
    foreach (QString bin, toConvert.split("|",QString::SkipEmptyParts)){
        toReturn.append(QString::number(bin.toInt(ok,2),10).append("|"));
        if(*ok==false){
            toReturn=toConvert;
            break;
        }
    }
    toReturn.chop(1);
    return toReturn;
}

QString convert::asciitoBin(QString toConvert)
{
    QString toReturn="";
    QString toAppend="";
    for(int i=0;i<toConvert.size();i++){
        toAppend=QString::number(toConvert.at(i).unicode(),2);
        for(int x=0;x<8-toAppend.size();x++){
            toAppend.prepend('0');
        }
            toReturn.append(toAppend+"|");
    }
    toReturn.chop(1);
    return toReturn;
}

QString convert::asciitoBin(QString toConvert, QString prepend, QString endOfAll)
{
    QString toReturn="";
    QString toAppend="";
    for(int i=0;i<toConvert.size();i++){
        toAppend=QString::number(toConvert.at(i).unicode(),2);
        for(int x=0;x<8-toAppend.size();x++){
            toAppend.prepend('0');
        }
            toReturn.append(prepend+toAppend+endOfAll);
    }
    toReturn.chop(1);
    return toReturn.append(endOfAll);
}

QString convert::hextoBin(bool *ok, QString toConvert)
{
    QString toReturn="";
    QString toAppend="";
    *ok=false;
    foreach (QString hex, toConvert.split("|",QString::SkipEmptyParts)){
        toAppend=QString::number(hex.toInt(ok,16),2);
        for(int x=0;x<8-toAppend.size();x++){
            toAppend.prepend('0');
        }
            toReturn.append(toAppend+"|");
        if(*ok==false){
            toReturn=toConvert;
            break;
        }
    }
    toReturn.chop(1);
    return toReturn;
}

QString convert::decimaltoBin(bool *ok, QString toConvert)
{
    QString toReturn="";
    QString toAppend="";
    *ok=false;
    foreach (QString decimal, toConvert.split("|",QString::SkipEmptyParts)){
        toAppend=QString::number(decimal.toInt(ok,10),2);
        for(int x=0;x<8-toAppend.size();x++){
            toAppend.prepend('0');
        }
            toReturn.append(toAppend+"|");
        if(*ok==false){
            toReturn=toConvert;
            break;
        }
    }
    toReturn.chop(1);
    return toReturn;
}
