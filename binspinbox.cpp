#include "binspinbox.h"

BinSpinBox::BinSpinBox(QWidget *parent) : QSpinBox(parent)
{
    setRange(0,255);
    validator = new QRegExpValidator(QRegExp("[0-1]{1,9}"),this);
}

QValidator::State BinSpinBox::validate(QString &text, int &pos) const
{
    return validator->validate(text,pos);
}

QString BinSpinBox::textFromValue(int value) const
{
    return QString::number(value,2).toUpper();
}

int BinSpinBox::valueFromText(const QString &text) const
{
    bool ok;
    return text.toInt(&ok,2);
}
