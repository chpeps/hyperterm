#ifndef UPDATERDIALOG_H
#define UPDATERDIALOG_H

#include <QApplication>
#include <QtGui>
#include <QPushButton>
#include <QLabel>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>
#include <QCoreApplication>
#include <QUrl>

class updaterDialog: public QWidget
{
public:
    updaterDialog();
protected:
    QPushButton *updateSoftware;
    QNetworkReply *reply;
    QString version; // Version actuel
    QString versionNew; // Version de la nouvelle version si elle existe
    QLabel label;

public slots:
    void updater(); //Slot de mise à jour
    void chargementTermine(bool erreur);
};

#endif // UPDATERDIALOG_H
