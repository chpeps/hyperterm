#include "about.h"
#include "ui_about.h"
#include "logger.h"
#include "QMessageBox"
#include "QFileDialog"

about::about(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::about)
{
    parentMain=parent;
    this->setWindowModality(Qt::ApplicationModal);
    QString version = QApplication::applicationVersion();

    ui->setupUi(this);
    this->setWindowFlags(Qt::SplashScreen);
    ui->label_2->setText(version);
    ui->pushButton_2->setVisible(false);
    ui->progressBar->setValue(0);
    ui->progressBar->setVisible(false);

    reply = manager.get(QNetworkRequest(QUrl("http://www.zokali.fr/ZhyperTerm/version.txt"))); // Url vers le fichier version.txt
    QEventLoop loop;
    QObject::connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    loop.exec();
    QString versionNew = reply->readAll().trimmed();
    Logger::log(Logger::DEBUG,Q_FUNC_INFO,"donnees distantes = "+versionNew);

    if(!version.contains("no load", Qt::CaseInsensitive)){
        Logger::log(Logger::DEBUG,Q_FUNC_INFO,"version ok to auto download !!");
        if (version != versionNew && !versionNew.isEmpty())
        {
            Logger::log(Logger::DEBUG,Q_FUNC_INFO,"version distante = "+versionNew);
            ui->label_4->setText("Nouvelle version Disponible : "+versionNew);
            ui->pushButton_2->setVisible(true);
            ui->pushButton_2->setStyleSheet("");
            if(this->isHidden()){
                QMessageBox::information(this,"Logiciel non à jour", "Une mise à jours est disponible.\n Version : "+versionNew);
                this->show();
            }
        }else if(versionNew.isEmpty()){
            Logger::log(Logger::DEBUG,Q_FUNC_INFO,"pas internet !!");
            ui->label_4->setText("Pas d'acces internet !");
            ui->pushButton_2->setVisible(false);
        }else{
            Logger::log(Logger::DEBUG,Q_FUNC_INFO,"version distante = "+versionNew);
            ui->label_4->setText("Logiciel à jour !");
            ui->pushButton_2->setVisible(false);
        }
    }else{
        Logger::log(Logger::DEBUG,Q_FUNC_INFO,"version no load !!");
        versionNew = versionNew.left(7).trimmed();
        if (version != versionNew && !versionNew.isEmpty())
        {
            Logger::log(Logger::DEBUG,Q_FUNC_INFO,"version no load distante = "+versionNew);
            ui->label_4->setText("Nouvelle version Disponible : "+versionNew+" rendez-vous sur le site internet.");
            if(this->isHidden()){
                QMessageBox::information(this,"Logiciel non à jour", "Une mise à jours est disponible.\n Version : "+versionNew);
                this->show();
            }
        }else{
            Logger::log(Logger::DEBUG,Q_FUNC_INFO,"version no load distante = "+versionNew);
            ui->label_4->setText("Logiciel à jour !");
            ui->pushButton_2->setVisible(false);
        }
    }
    //for debug update
    //ui->pushButton_2->setVisible(true);
}

about::~about()
{
    delete ui;
}

void about::on_pushButton_clicked()
{
    this->hide();
}

void about::on_pushButton_2_clicked()
{
    ui->progressBar->setVisible(true);
    ui->pushButton_2->setEnabled(false);
    ui->progressBar->setStyleSheet("");
#ifdef _WIN32 //http://www.zokali.fr/ZhyperTerm/last/windows/HyperTerm.exe
    QString versionDirectory("windows");
    QString extension(".exe");
#elif __APPLE__ // http://www.zokali.fr/ZhyperTerm/last/mac/ZhyperTerm.app/Contents/MacOS/HyperTerm
    QString versionDirectory("mac");
    QString extension(".app/Contents/MacOS/HyperTerm");
#elif __linux
    QString versionDirectory("linux");
    QString extension("");
#endif

    reply = manager.get(QNetworkRequest(QUrl("http://zokali.fr/ZhyperTerm/last/"+versionDirectory+"/HyperTerm"+extension)));

    QObject::connect(reply, SIGNAL(finished()), this, SLOT(enregistrer()));
    QObject::connect(reply, SIGNAL(downloadProgress(qint64, qint64)), this, SLOT(progressionTelechargement(qint64, qint64)));

}

void about::progressionTelechargement(qint64 bytesReceived, qint64 bytesTotal)
{
    if (bytesTotal != -1)
    {
        ui->progressBar->setRange(0, bytesTotal);
        ui->progressBar->setValue(bytesReceived);
        Logger::log(Logger::DEBUG,Q_FUNC_INFO,"Telechargement ..."+QString::number(bytesReceived));
    }
}

void about::enregistrer()
{
    reply->deleteLater();
    QMessageBox msgBox;
#ifdef _WIN32
    QString extension(".exe");
#elif __APPLE__
    QString extension("");
#elif __linux
    QString extension("");
#endif
    Logger::log(Logger::DEBUG,Q_FUNC_INFO,"application path = "+QApplication::applicationFilePath());
    QFile isPresent("pastApplication.toDelete");
        if(isPresent.exists())
            isPresent.remove();
    QFile lastversion(QApplication::applicationFilePath());
    lastversion.rename("pastApplication.toDelete");
    lastversion.copy(QApplication::applicationName().split("/",QString::SkipEmptyParts).last());
    this->hide();
    QFile newversion(QApplication::applicationFilePath());
    if ( newversion.open(QIODevice::WriteOnly) )
    {
        newversion.write(reply->readAll());
        newversion.close();

    }else{
        Logger::log(Logger::DEBUG,Q_FUNC_INFO,"Failed to open "+newversion.fileName()+"for write: "+newversion.errorString());

        QMessageBox::critical(this, "Fin de téléchargement", "Impossible de faire la mise à jour directement.\nMerci de sauvegarder le fichier telecharger et ensuite de le remplacer.");
        QString fn = QFileDialog::getSaveFileName(NULL, "Save as...",QApplication::applicationFilePath(), "Programme File (*"+extension+")");

        QFile *newversionSaveAs=new QFile(fn);
        if ( newversionSaveAs->open(QIODevice::WriteOnly) )
        {
            newversionSaveAs->write(reply->readAll());
            newversionSaveAs->close();
        }else{
            Logger::log(Logger::DEBUG,Q_FUNC_INFO,"Failed to open "+newversionSaveAs->fileName()+"for write: "+newversionSaveAs->errorString());
            lastversion.rename("HyperTerm.exe");
            QMessageBox::critical(this, "Fin de téléchargement", "Probleme impossible d'ouvrir le fichier\nConnectez-vous sur le site internet.");
            return;
        }
        msgBox.setText("mise a jour");
        msgBox.setInformativeText("le nouveau programme a ete sauvegarder.voulez-vous quitter le programme pour le remplacer ?");
        msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
        msgBox.setDefaultButton(QMessageBox::Ok);
        if(msgBox.exec() == QMessageBox::Ok){
            emit exitApp();
        }
    }
    ui->progressBar->setVisible(false);


    msgBox.setText("mise a jour");
    msgBox.setInformativeText("le programme a ete mis a jour.voulez-vous redemarer le programme?");
    msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Ok);
    if(msgBox.exec() == QMessageBox::Ok){
        emit reboot();
    }
}
