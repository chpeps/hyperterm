#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include "settingcmdwindow.h"
#include "qextserialport.h"
#include "qextserialenumerator.h"
#include <QStandardItemModel>
#include "uart.h"
#include <QLabel>
#include "processSending.h"
#include <QMessageBox>
#include "asciichart.h"
#include "preferences.h"
#include "about.h"
#include "hexspinbox.h"
#include "binspinbox.h"

namespace Ui {
class MainWindow;
}
class QTimer;
class QextSerialPort;
class QextSerialEnumerator;

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    bool eventFilter(QObject* object, QEvent* event);



private Q_SLOTS:
    void addInTree(QTreeWidgetItem *itemEdited);
    void addInTree(QString name, QString cmd, QString type, QString time);
    void editedInTree(QString name,QString cmd,QString type,int row);

    void on_pushButtonAdd_clicked();

    void on_pushButtonRemove_clicked();

    void on_pushButtonEdit_clicked();

    void on_tableView_doubleClicked(const QModelIndex &index);

    void onPortAddedOrRemoved();
    void onPortNameChanged(const QString &name);
    void onBaudRateChanged(int idx);
    void onParityChanged(int idx);
    void onDataBitsChanged(int idx);
    void onStopBitsChanged(int idx);
    void onQueryModeChanged(int idx);
    void onOpenCloseButtonClicked();
    void onSendButtonClicked();
    void onReadyRead();

    void on_actionNew_triggered();

    void on_spinBoxTimeout_valueChanged(int arg1);

    void on_pushButtonClearSend_clicked();

    void on_pushButtonClearReceived_clicked();
    void timerToggleRxd();
    void timerToggleTxd();
    void warningConnectButton();
    void dsrChanged(bool state);
    void onBytesWritten(qint64 size);

    void on_lineEditToSend_returnPressed();

    void on_comboBoxComPort_currentIndexChanged(const QString &arg1);

    void on_pushButtonAddFolder_clicked();

    void on_pushButton_clicked();
    void sendText();
    void sendText(QString dataToSend);
    void sendTextReturn();

    void on_spinBoxInterCMD_valueChanged(int arg1);

    void on_pushButtonMask_clicked();

    void refreshingbutton();

    void on_actionOuvrir_une_configuration_triggered();

    void on_actionSave_configuration_triggered();

    void on_actionNouvelle_configuration_triggered();

    void on_actionSave_as_Configuration_triggered();

    void on_actionNouvelle_commande_triggered();

    void on_actionNouveau_fichier_triggered();

    void on_actionTable_ASCII_triggered();

    void triggeredHistorique(QAction* action);

    void on_actionPr_f_rences_triggered();

    void pref_ShowStatusChange(bool arg1);
    void pref_ShowExtendChange(bool arg1);
    void pref_ShowCMDTableChange(bool arg1);
    void pref_SplitRecepAndTransChange(bool arg1);

    void on_actionAbout_triggered();

    void on_pushButtonDTR_clicked();

    void on_pushButtonRTS_clicked();

    void on_checkBoxWhile_toggled(bool checked);

    void on_spinBoxTime_valueChanged(int arg1);

    void on_toolButtonFile_clicked();

    void colorTextChange(QColor newcolorSend,QColor oldcolorSend,QColor newColorReceived,QColor oldcolorReceived);

    void on_checkBoxSendAsTyping_toggled(bool checked);

    void exitApp();
    void reboot();

    void on_pushButtonConnect_clicked();

    void on_comboBoxFlowControl_currentIndexChanged(int index);

    void buttonGroupStateChange(int index);

    void buttonGroup_2StateChange(int index);

    void on_lineEditCustomEndLine_editingFinished();

    void on_checkBoxEraseAfterSend_toggled(bool checked);

    void on_checkBoxTimeLine_toggled(bool checked);

    void on_checkBoxAppendNewLine_toggled(bool checked);

    void on_spinBoxDecimal_valueChanged(int arg1);

    void hexBoxDecimal_valueChanged(int arg1);

    void binBoxDecimal_valueChanged(int arg1);

    void on_lineEditcharacter_textChanged(const QString &arg1);

private:
    Ui::MainWindow *ui;
    void closeWidgets();
    void toggleLED(QLabel *led);
    void getAllSettings();

    preferences *pref;
    about *aboutScreen;

    HexSpinBox *hexspinbox;
    BinSpinBox *binspinbox;

    settingCmdWindow* settingcmdwindow;
    QStandardItemModel *model;
    QextSerialPort *port;
    QextSerialEnumerator *enumerator;
    QTimer *timer;
    QTimer *toggleurRx;
    QTimer *toggleurTx;
    QTimer *warningConnect;
    QTimer *refreshButtonTimer;
    QTimer *sendTextWhile;
    Process *toSendProcess;
    QString actualPathForSave;
    QString pastDataSend;

    bool RTSState;
    bool DTRState;

    const static QString savePortCOM;
    const static QString saveBaudRate;
    const static QString saveStopBits;
    const static QString saveParity;
    const static QString saveDataBits;
    const static QString saveQueryMode;
    const static QString saveTimeout;
    const static QString saveFlowControl;
    const static QString saveDataFormat;
    const static QString saveNewLineSettings;
    const static QString saveNewLineSettingsCustom;
    const static QString saveEraseAfterSend;
    const static QString saveTimeLine;
    const static QString saveAppendNewLine;
    const static QString saveEnvoyerEnBoucle;
    const static QString saveEnvoyerEnBoucleTime;
    const static QString saveSendAsTyping;
    const static QString settingGroup;

    QSettings *settingsRegister;
};

#endif // MAINWINDOW_H
