#ifndef SETTINGCMDWINDOW_H
#define SETTINGCMDWINDOW_H

#include <QWidget>
#include <QAbstractButton>
#include <QTreeWidgetItem>

namespace Ui {
class settingCmdWindow;
}

class settingCmdWindow : public QWidget
{
    Q_OBJECT
    
public:
    explicit settingCmdWindow(QWidget *parent = 0);
    ~settingCmdWindow();
    void setName(QString name);
    void setCMD(QString CMD);
    void setType(QString type);
    void setItemToEdit(QTreeWidgetItem *itemEdited);
    bool isEditMode;
    int editedRow;
    QString name;
    QString CMD;
    QString typeCMD;
    QTreeWidgetItem *itemEdited;

signals:
    void dataAdded(QTreeWidgetItem *itemEdited);
    void dataAdded(QString name,QString cmd,QString type, QString time);
    void dataEdited(QString name,QString cmd,QString type,int row);
    
private slots:
    void on_pushButtonAdd_clicked();
    void groupButtonChange(int button);

    void on_lineEditCMD_textChanged(const QString &arg1);

    void on_lineEditName_textChanged(const QString &arg1);

    void on_lineEditCMD_returnPressed();

private:
    static const int ASCII=-2;
    static const int HEX=-3;
    static const int DECIMAL=-4;
    static const int BIN=-5;

    Ui::settingCmdWindow *ui;
    void keyPressEvent(QKeyEvent* keyEvent);
    int pastButtonValue;
};

#endif // SETTINGCMDWINDOW_H
