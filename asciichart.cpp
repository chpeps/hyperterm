#include "asciichart.h"
#include "ui_asciichart.h"
#include <QDebug>

ASCIIChart::ASCIIChart(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ASCIIChart)
{
    ui->setupUi(this);

    QStringList HorizontalheaderLabel;
    QStringList VerticalheaderLabel;

    for(int i =0;i<16;i++){
        VerticalheaderLabel<<QString::number(i,16).toUpper();
        for(int x = 0;x<16;x++){
            HorizontalheaderLabel<<QString::number(x,16).toUpper();
            qDebug()<<((i*16)+x);
            QString toolTips ="";
            QTableWidgetItem* widgetItem =  new QTableWidgetItem(QChar((i*16)+x),Qt::DisplayRole);
            toolTips+="caractere = '"+QString(QChar((i*16)+x))+"'\n";
            toolTips+="decimal = "+QString::number((i*16)+x)+"\n";
            toolTips+="hexadecimal = "+QString::number((i*16)+x,16)+"\n";
            toolTips+="binaire = "+QString::number((i*16)+x,2)+"\n";
            widgetItem->setToolTip(toolTips);
            ui->tableWidget->setItem(i,x,widgetItem);

        }
    }
    //ui->tableWidget->
    ui->tableWidget->setHorizontalHeaderLabels(HorizontalheaderLabel);
    ui->tableWidget->setVerticalHeaderLabels(VerticalheaderLabel);
}

ASCIIChart::~ASCIIChart()
{
    delete ui;
}

void ASCIIChart::on_tableWidget_cellClicked(int row, int column)
{
    QString textLabel ="";
    int i = row;
    int x = column;
    textLabel+="caractere = '"+QString(QChar((i*16)+x))+"'\n";
    textLabel+="decimal = "+QString::number((i*16)+x)+"\n";
    textLabel+="hexadecimal = "+QString::number((i*16)+x,16)+"\n";
    textLabel+="binaire = "+QString::number((i*16)+x,2)+"\n";

    newlabel.setText(textLabel);
    newlabel.setFixedHeight(80);
    newlabel.setFixedWidth(150);
    newlabel.move(QCursor::pos());
    newlabel.setWindowFlags(Qt::WindowStaysOnTopHint);//Qt::SplashScreen);
    newlabel.show();
}
