#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QDebug"
#include <QTableWidget>
#include "logger.h"
#include "convert.h"
#include "xmldom.h"
#include "updaterdialog.h"
#include "QFileDialog"


const QString MainWindow::savePortCOM="savePortCOM";
const QString MainWindow::saveBaudRate="saveBaudRate";
const QString MainWindow::saveStopBits="saveStopBits";
const QString MainWindow::saveParity="saveParity";
const QString MainWindow::saveDataBits="saveDataBits";
const QString MainWindow::saveQueryMode="saveQueryMode";
const QString MainWindow::saveTimeout="saveTimeout";
const QString MainWindow::saveFlowControl="saveFlowControl";

const QString MainWindow::saveDataFormat="saveDataFormat";
const QString MainWindow::saveNewLineSettings="saveNewLineSettings";
const QString MainWindow::saveNewLineSettingsCustom="saveNewLineSettingsCustom";
const QString MainWindow::saveEraseAfterSend="saveEraseAfterSend";
const QString MainWindow::saveTimeLine="saveTimeLine";
const QString MainWindow::saveAppendNewLine="saveAppendNewLine";
const QString MainWindow::saveEnvoyerEnBoucle="saveEnvoyerEnBoucle";
const QString MainWindow::saveEnvoyerEnBoucleTime="saveEnvoyerEnBoucleTime";
const QString MainWindow::saveSendAsTyping="saveSendAsTyping";
const QString MainWindow::settingGroup="MainWindow";


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    //setWindowFlags(Qt::WindowStaysOnBottomHint);
    settingsRegister=new QSettings("ZOKALI","HyperTerm");
    //settingsRegister->beginGroup(settingGroup);

    bool stateBool=settingsRegister->value("Prefs/logFile","true").toBool();
    Logger::remove();
    Logger::stopFileLog(!stateBool);
    Logger::log(Logger::DEBUG,Q_FUNC_INFO,"application path :"+QApplication::applicationFilePath());

    Logger::log(Logger::DEBUG,Q_FUNC_INFO,"Programme START");
    Logger::log(Logger::DEBUG,Q_FUNC_INFO,"application path = "+ QApplication::applicationFilePath());

    actualPathForSave="";
    RTSState = false;
    DTRState = false;

    ui->setupUi(this);//reglage du formulaire de l'UI
    installEventFilter(this);//on installe "event filter"

    ui->spinBoxDecimal->setMinimumWidth(100);

    hexspinbox = new HexSpinBox(ui->frameHex);
    hexspinbox->setMinimumWidth(100);
    QObject::connect(hexspinbox,SIGNAL(valueChanged(int)),this,SLOT(hexBoxDecimal_valueChanged(int)));
    binspinbox = new BinSpinBox(ui->frameBin);
    binspinbox->setMinimumWidth(200);
    QObject::connect(binspinbox,SIGNAL(valueChanged(int)),this,SLOT(binBoxDecimal_valueChanged(int)));
    QGridLayout *gbhex = new QGridLayout;
    gbhex->addWidget(hexspinbox, 0, 0);
    ui->frameHex->setLayout(gbhex);

    QGridLayout *gbbin = new QGridLayout;
    gbbin->addWidget(binspinbox, 0, 0);
    ui->frameBin->setLayout(gbbin);

    pref = new preferences(this);
    aboutScreen = new about(this);

    QObject::connect(aboutScreen,SIGNAL(exitApp()),this,SLOT(exitApp()));
    QObject::connect(aboutScreen,SIGNAL(reboot()),this,SLOT(reboot()));

    QObject::connect(pref,SIGNAL(ShowExtendChange(bool)),this,SLOT(pref_ShowExtendChange(bool)));
    QObject::connect(pref,SIGNAL(ShowStatusChange(bool)),this,SLOT(pref_ShowStatusChange(bool)));
    QObject::connect(pref,SIGNAL(ShowCMDTableChange(bool)),this,SLOT(pref_ShowCMDTableChange(bool)));
    QObject::connect(pref,SIGNAL(SplitRecepAndTransChange(bool)),this,SLOT(pref_SplitRecepAndTransChange(bool)));
    QObject::connect(pref,SIGNAL(newColor(QColor,QColor,QColor,QColor)),this,SLOT(colorTextChange(QColor,QColor,QColor,QColor)));

    pref->getAllSettings();

    //[0] ici on creer un menu d'action sur le plainTextEditSend
    /* ui->plainTextEditSend->setContextMenuPolicy(Qt::ActionsContextMenu);
    QAction *actionAdd =new QAction("Selection Value ...",this);
    ui->plainTextEditSend->addAction(actionAdd);*/
    //![0]

    //on regle la setting commande windows qui permet de creer des commandes RS232
    settingcmdwindow=new settingCmdWindow(this);//init
    settingcmdwindow->setWindowModality(Qt::NonModal);// la qdialog est non modal

    //[1] Init la table qui permet de stocker les commandes RS232
    ui->tableView->setDragEnabled(true);
    ui->tableView->setDragDropMode(QTableView::InternalMove);
    ui->tableView->setDefaultDropAction(Qt::MoveAction);
    ui->tableView->setAcceptDrops(true);

    //on creer une liste contenant le nom des colonnes
    QStringList list;
    list<<"Name"<<"CMD"<<"Time(ms)"<<"Type";
    ui->tableView->setHeaderLabels(list);
    ui->tableView->hideColumn(3);

    ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);//on autorise l'utlisateur a selectionner que une ligne
    //ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);//on interdit l'edition des cases
    //ui->tableView->setColumnWidth(1,150);//on regle la taille de la deuxieme colonne
    ui->tableView->setColumnCount(4);
    /*QList<QTreeWidgetItem *> items;
    for (int i = 0; i < 10; ++i){
        QStringList list;
        list<<"Name"+QString::number(i)<<"CMD"+QString::number(i)<<"100"<<"Type";
        QTreeWidgetItem *item = new QTreeWidgetItem(ui->tableView, list);
        item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsDragEnabled);
        items.append(item);
    }*/

    //QTreeWidgetItem *test=new QTreeWidgetItem((QTreeWidget*)0, QStringList(QString("item: %1")));
    //ui->tableView->insertTopLevelItem(1,test);
    //![1]

    //on connect les signaux de "settingcmdwindow"
    QObject::connect(settingcmdwindow,SIGNAL(dataAdded(QString,QString,QString,QString)),this,SLOT(addInTree(QString,QString,QString,QString)));
    QObject::connect(settingcmdwindow,SIGNAL(dataAdded(QTreeWidgetItem*)),this,SLOT(addInTree(QTreeWidgetItem*)));
    QObject::connect(settingcmdwindow,SIGNAL(dataEdited(QString,QString,QString,int)),this,SLOT(editedInTree(QString,QString,QString,int)));

    //on initialise le timer
    timer = new QTimer(this);
    timer->setInterval(10);//10 ms

    //[2] reglage du qextserialport
    PortSettings settings = {BAUD9600, DATA_8, PAR_NONE, STOP_1, FLOW_OFF, 10};//default settings
    port = new QextSerialPort(ui->comboBoxComPort->currentText(), settings, QextSerialPort::Polling);//default setting

    //reglage du enumerateur des ports RS232
    enumerator = new QextSerialEnumerator(this);
    enumerator->setUpNotifications();
    //recuperation de tous les ports RS232 disponible et on las ajout dans la combobox conenant les ports COM
    ui->comboBoxComPort->blockSignals(true);
    foreach (const QextPortInfo info, enumerator->getPorts()) {
        ui->comboBoxComPort->addItem(info.portName);
    }
    ui->comboBoxComPort->blockSignals(false);

    //initialisation de toutes les combobox
    uart().setComboboxBaudrate(ui->comboBoxBaudRate);//init des baudrates
    uart().setComboboxDatabits(ui->comboBoxDataBits);//init du nombre de bits dans une trame
    uart().setComboboxParity(ui->comboBoxParity);//init des paritées
    uart().setComboboxStopBits(ui->comboBoxStopBits);//init des stops bits
    uart().setComboboxFlowControl(ui->comboBoxFlowControl);//init du control de flux
    uart().setComboboxQueryMode(ui->comboBoxQueryMode);//init du type de reception des trames

    toSendProcess=new Process(this,port,ui->tableView,1,Process::READNOTDEFINED);
    QObject::connect(toSendProcess,SIGNAL(setMaximum(int)),ui->progressBarAllCMD,SLOT(setMaximum(int)));
    QObject::connect(toSendProcess,SIGNAL(setValue(int)),ui->progressBarAllCMD,SLOT(setValue(int)));
    QObject::connect(toSendProcess,SIGNAL(toSendText(QString)),this,SLOT(sendText(QString)));

    //creation des connexion relatif au port RS232
    QObject::connect(ui->pushButtonReloadPort, SIGNAL(clicked()),this,SLOT(onPortAddedOrRemoved()));//si l"utilisateur clique sur le bouton pour recharger les ports
    QObject::connect(enumerator, SIGNAL(deviceDiscovered(QextPortInfo)),this,SLOT(onPortAddedOrRemoved()));//si il y a un nouveau port COM
    QObject::connect(enumerator, SIGNAL(deviceRemoved(QextPortInfo)),this,SLOT(onPortAddedOrRemoved()));//si un port COM a disparue
    QObject::connect(ui->comboBoxBaudRate, SIGNAL(currentIndexChanged(int)),this,SLOT(onBaudRateChanged(int)));//si la valeur du baudrate a changée
    QObject::connect(ui->comboBoxParity, SIGNAL(currentIndexChanged(int)),this,SLOT(onParityChanged(int)));//si la valeur de la partité a changée
    QObject::connect(ui->comboBoxDataBits, SIGNAL(currentIndexChanged(int)),this,SLOT(onDataBitsChanged(int)));//si la valeur des bits de données on changés
    QObject::connect(ui->comboBoxStopBits, SIGNAL(currentIndexChanged(int)),this,SLOT(onStopBitsChanged(int)));//si la valeur des stops bits on changés
    QObject::connect(ui->comboBoxQueryMode, SIGNAL(currentIndexChanged(int)),this,SLOT(onQueryModeChanged(int)));//si la methode de reception des trames a changée
    //QObject::connect(ui->comboBoxComPort, SIGNAL(editTextChanged(QString)),this,SLOT(onPortNameChanged(QString)));
    QObject::connect(ui->pushButtonConnect, SIGNAL(clicked()),this,SLOT(onOpenCloseButtonClicked()));//si l'utilisateur clique sur le bouton "connecter"
    QObject::connect(ui->pushButtonDisconnect, SIGNAL(clicked()),this,SLOT(onOpenCloseButtonClicked()));//si l'utilisateur clique sur le bouton "deconecter"
    QObject::connect(ui->pushButtonSend, SIGNAL(clicked()),this,SLOT(onSendButtonClicked()));//si l'utilisateur clique sur le bouton "send"
    QObject::connect(timer, SIGNAL(timeout()),this,SLOT(onReadyRead()));//si le timer polling est en timeout
    QObject::connect(port, SIGNAL(readyRead()),this,SLOT(onReadyRead()));//si de la donnée est disponible sur le port
    QObject::connect(port,SIGNAL(dsrChanged(bool)),this,SLOT(dsrChanged(bool)));//si l'etat du DSR a changé
    QObject::connect(port,SIGNAL(bytesWritten(qint64)),this,SLOT(onBytesWritten(qint64)));//si des bits on ete ecrit
    //![2]

    QObject::connect(ui->buttonGroup,SIGNAL(buttonClicked(int)),this,SLOT(buttonGroupStateChange(int)));
    QObject::connect(ui->buttonGroup_2,SIGNAL(buttonClicked(int)),this,SLOT(buttonGroup_2StateChange(int)));


    //timer permettant de changer l'etat de la led RX
    toggleurRx =new QTimer(this);
    toggleurRx->setInterval(1);
    QObject::connect(toggleurRx,SIGNAL(timeout()),this,SLOT(timerToggleRxd()));//si le timer RX est en timeout

    //timer permettant de changer l'etat de la led TX
    toggleurTx =new QTimer(this);
    toggleurTx->setInterval(1);
    QObject::connect(toggleurTx,SIGNAL(timeout()),this,SLOT(timerToggleTxd()));//si le timer TX est en timeout

    //timer permetant d'afficher un warning lor d'un probleme de connection
    warningConnect = new QTimer(this);
    warningConnect->setInterval(10);//reglage de l'intervale de temps
    QObject::connect(warningConnect,SIGNAL(timeout()),this,SLOT(warningConnectButton()));//si le timer warning connection est en timeout

    refreshButtonTimer = new QTimer(this);
    refreshButtonTimer->setInterval(10);
    QObject::connect(refreshButtonTimer,SIGNAL(timeout()),this,SLOT(refreshingbutton()));

    sendTextWhile = new QTimer(this);
    sendTextWhile->setInterval(ui->spinBoxTime->value());
    QObject::connect(sendTextWhile,SIGNAL(timeout()),this,SLOT(sendText()));

    getAllSettings();

    //ui->spinBoxDecimal->
}

MainWindow::~MainWindow()
{
    closeWidgets();
    aboutScreen->close();
    delete ui;
}
/**
 * @brief MainWindow::eventFilter
 *\section1 Usage
 * permet de recuperer les evenement relatif a la fentre principale
 * @param object
 * @param event
 * @return false pour redonner la main a la fenetre principale
 */
bool MainWindow::eventFilter(QObject *object, QEvent *event)
{
    //si l'utilisateur clique sur la fenetre principale alors on masque les widgets
    if ((event->type() == QEvent::WindowActivate ||event->type() ==  QEvent::MouseButtonRelease) && this == object)
    {
        Logger::log(Logger::DEBUG,Q_FUNC_INFO,"Filtre d'event event->type() == QEvent::WindowActivate");
        closeWidgets();//fermeture des widgets
    }
    return false;//return false pour redonner la main a la fenetre principale
}

void MainWindow::addInTree(QTreeWidgetItem *itemEdited)
{
    ui->tableView->addTopLevelItem(itemEdited);
    if(ui->tableView->topLevelItemCount()>0){
        ui->pushButtonEdit->setEnabled(true);
    }else{
        ui->pushButtonEdit->setEnabled(false);
    }
}
/**
 * @brief MainWindow::closeWidgets
 *\section1 Usage
 *permet de fermer tous les widgets
 */
void MainWindow::closeWidgets()
{
    Logger::log(Logger::DEBUG,Q_FUNC_INFO,"All widget close");
    settingcmdwindow->hide();//on masque la fenetre de creation de CMD
    this->activateWindow();//on reactive la fenetre principal
}

int numberTx=0;
/**
 * @brief MainWindow::sendText
 *\section1 Usage
 *permet d'envoyer sur la liason RS232 les données avec le bon format
 * @param dataToSend
 */
void MainWindow::sendText(QString dataToSend)
{
    QString tosend="";
    //si le port est ouvert et que dataTosend n'est pas vide alors ...
    if (port->isOpen() && !dataToSend.isEmpty()){
        QString toAppend="";
        //si on doit ajouter un fin de ligne alors...
        if(ui->checkBoxAppendNewLine->isChecked() && ui->checkBoxAppendNewLine->isEnabled()){
            if(ui->radioButtonCRLF->isChecked())//si CR/LF(\r\n) boutongroup est selectionné
                toAppend="\r\n";
            else if(ui->radioButtonLF->isChecked())//si LF(\n) boutongroup est selectionné
                toAppend="\n";
            else if(ui->radioButtonCR->isChecked())//si CR(\r) boutongroup est selectionné
                toAppend="\r";
            else if(ui->radioButtonCustom->isChecked())//si custom boutongroup est selectionné
                toAppend=ui->lineEditCustomEndLine->text();
        }
        tosend=dataToSend+toAppend;

        Logger::log(Logger::DEBUG,Q_FUNC_INFO,"data send ="+tosend);

        ui->plainTextEditSend->moveCursor(QTextCursor::End);//on replace le cursor a la fin du text
        QString dataToplot =tosend;
        if(ui->radioButtonBIN->isChecked()){
            dataToplot=convert::asciitoBin(tosend,"0b"," ");
        }else if(ui->radioButtonDEC->isChecked()){
            dataToplot=convert::asciitoDecimal(tosend,""," ");
        }else if(ui->radioButtonHEX->isChecked()){
            dataToplot=convert::asciitoHex(tosend,"0x"," ");
        }

        if(ui->checkBoxTimeLine->isChecked() && ui->checkBoxTimeLine->isEnabled()){
            // dataToplot = dataToplot.simplified();
            dataToplot.prepend("["+QDateTime::currentDateTime().toString("dd/MM/yy hh:mm:ss,zzz")+"] ").append("\r\n");
        }

        /*
         value = replace(value, "\r\n","<BR>&nbsp;&nbsp;&nbsp;&nbsp;");
         value = replace(value, "\r","<BR>&nbsp;&nbsp;");
         value = replace(value, "\n","<BR>&nbsp;&nbsp;");
        */

        dataToplot.replace("\r\n","<BR>");
        dataToplot.replace("\r","<BR>");
        dataToplot.replace("\n","<BR>");
        dataToplot.replace(" ","&nbsp;");

        dataToplot.prepend("<FONT COLOR=\""+pref->couleurSend.name()+"\">");
        dataToplot.append("</font>");
        Logger::log(Logger::DEBUG,Q_FUNC_INFO,"data to plot ="+dataToplot);
        ui->plainTextEditSend->moveCursor(QTextCursor::End);
        ui->plainTextEditSend->insertHtml(dataToplot);//on insert les nouvelles données
        port->write(tosend.toStdString().c_str());//on l'ecrit sur le port
        port->flush();//on force le vidage du buffer

        pastDataSend=dataToSend;

        if(ui->checkBoxWhile->isChecked() && ui->checkBoxWhile->isEnabled())
            sendTextWhile->start();

        //creation de l'historique des commandes
        QAction *historique = new QAction(ui->menuHistorique_des_CMDs);
        historique->setText(dataToSend);
        QObject::connect(ui->menuHistorique_des_CMDs,SIGNAL(triggered(QAction*)),this,SLOT(triggeredHistorique(QAction*)));
        if(ui->menuHistorique_des_CMDs->actions().count()>30){
            ui->menuHistorique_des_CMDs->actions().at(0)->deleteLater();
        }
        ui->menuHistorique_des_CMDs->addAction(dataToSend);

        if(ui->checkBoxEraseAfterSend->isChecked())//si le erase after send est cocher alors ...
            ui->lineEditToSend->clear();//on efface le line edit

        numberTx+=tosend.size()*8;//nombre de bit envoyé
        if(!toggleurTx->isActive()){//si le timer n'est pas actif on lance le timer
            toggleurTx->start();
        }
    }else if(!port->isOpen()){ //si le port est fermer alors on affiche en rouge le bouton connect
        warningConnect->start();//on lance le timer
    }else{//sinon (datatosend est vide)
        ui->lineEditToSend->setFocus();//on met le cursor d'ecriture sur le ligne edite
    }
}

void MainWindow::sendTextReturn()
{
    if(ui->radioButtonCRLF->isChecked())//si CR/LF(\r\n) boutongroup est selectionné
        sendText("\r\n");
    else if(ui->radioButtonLF->isChecked())//si LF(\n) boutongroup est selectionné
        sendText("\n");
    else if(ui->radioButtonCR->isChecked())//si CR(\r) boutongroup est selectionné
        sendText("\r");
    else if(ui->radioButtonCustom->isChecked())//si custom boutongroup est selectionné
        sendText(ui->lineEditCustomEndLine->text());
}


int alpha=255;
/**
 * @brief MainWindow::warningConnectButton
 *\section1 Usage
 *permet d'afficher a l'utilisateur que aucun port n'est connecté
 */
void MainWindow::warningConnectButton(){
    ui->pushButtonConnect->setStyleSheet(QString("QPushButton {background-color: rgba(255, 0, 0, %1) }").arg(alpha-=5));
    ui->pushButtonConnect->setAutoFillBackground(true);//on affiche le background
    if(alpha==0){//si le alpha est inferieur a zero
        warningConnect->stop();//on arrete le timer
        ui->pushButtonConnect->setStyleSheet("");//on remet le style par defaut du bouton
        ui->pushButtonConnect->setAutoFillBackground(false);//on desactive le background
        alpha=255;
    }
}
/**
 * @brief MainWindow::dsrChanged
 *\section1 Usage
 *si l'etat de la ligne DSR(RS232) a changer
 * @param state
 */
void MainWindow::dsrChanged(bool state)
{

    //on fonction de sont etat on afficher une couleur vert(TRUE) ou rouge(FALSE)
    if(!state)
        ui->labelDSRLed->setPixmap(QPixmap(QString::fromUtf8(":/images/images/circle_red.png")));
    else
        ui->labelDSRLed->setPixmap(QPixmap(QString::fromUtf8(":/images/images/circle_green.png")));
}
/**
 * @brief MainWindow::onBytesWritten
 *\section1 Usage
 *RFU
 * @param size
 */
void MainWindow::onBytesWritten(qint64 size)
{
    Logger::log(Logger::DEBUG,Q_FUNC_INFO,"nb de byte written = "+QString::number(size));
}

/**
 * @brief MainWindow::addInTree
 *\section1 Usage
 *permet d'ajouter une CMD dans l'afficheru de commande
 * @param name nom de la comande
 * @param cmd commande
 * @param type type de commande ASCII HEX DECIMAL BINAIRE
 */
void MainWindow::addInTree(QString name, QString cmd, QString type, QString time)
{
    Logger::log(Logger::DEBUG,Q_FUNC_INFO,"data in tree added (name="+name+", cmd="+cmd+")");
    QStringList list;
    list<<name<<cmd<<time<<type;
    QTreeWidgetItem *item;
    if(ui->tableView->currentItem()!=NULL){
        if(ui->tableView->currentItem()->flags()!=(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsDragEnabled)){
            item = new QTreeWidgetItem(ui->tableView->currentItem(), list);
        }else{
            item = new QTreeWidgetItem(ui->tableView, list);
        }
    }else{
        item = new QTreeWidgetItem(ui->tableView, list);
    }
    item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsDragEnabled);
    if(ui->tableView->topLevelItemCount()>0){
        ui->pushButtonEdit->setEnabled(true);
    }else{
        ui->pushButtonEdit->setEnabled(false);
    }

    ui->tableView->resizeColumnToContents(0);
    //ui->tableView->currentItem()->parent()->addChild(item);
    /*
    QTableWidgetItem *item0=new QTableWidgetItem;//init de la premier case de la nouvelle ligne du tableau
    item0->setText(name);//on regle la valeur de la case
   // int nbrow=model->rowCount();//on recupere le nombre de ligne
    int nbrow=ui->tableView->rowCount();//on recupere le nombre de ligne
    nbrow=1;
    //model->setItem(nbrow, 0, item0);//on l'ajoute dans le tableau dans une nouvelle ligne case 1
    ui->tableView->setItem(nbrow, 0, item0);//on l'ajoute dans le tableau dans une nouvelle ligne case 1
    QTableWidgetItem *item1=new QTableWidgetItem;//init de la deuxieme case de la nouvelle ligne du tableau
    item1->setText(cmd);//on regle la valeur de la case
    //model->setItem(nbrow, 1, item1);//on l'ajoute dans le tableau dans une nouvelle ligne case 2
     ui->tableView->setItem(nbrow, 1, item1);//on l'ajoute dans le tableau dans une nouvelle ligne case 2
    QTableWidgetItem *item2=new QTableWidgetItem;//init de la Troisieme case de la nouvelle ligne du tableau
    item2->setText(type);//on regle la valeur de la case
    //model->setItem(nbrow, 2, item2);//on l'ajoute dans le tableau dans une nouvelle ligne case 3
    ui->tableView->setItem(nbrow, 2, item2);//on l'ajoute dans le tableau dans une nouvelle ligne case 3
    ui->tableView->insertRow(1);
    ui->tableView->repaint();
    //si une ligne est present dans le tableau on active la possibilité de la modifier
    //if(model->rowCount()>0){
    if(ui->tableView->rowCount()>0){
        ui->pushButtonEdit->setEnabled(true);
    }else{
        ui->pushButtonEdit->setEnabled(false);
    }

    ui->tableView->resizeColumnsToContents();//regle la taille des cases au contenue
    int widthColum = ui->tableView->columnWidth(1)+ui->tableView->columnWidth(0);
    //si la column n'est pas assez grand alors on change la taille par defaut
    if(widthColum<ui->tableView->width()){
        ui->tableView->setColumnWidth(1,(ui->tableView->width()-ui->tableView->columnWidth(0)));
    }
*/
}

void MainWindow::editedInTree(QString name, QString cmd, QString type, int row)
{
    row=row+1;
    Logger::log(Logger::DEBUG,Q_FUNC_INFO,"data edited in tree added (name="+name+", cmd="+cmd+", type="+type+", row="+QString::number(row)+")");
    /*
    QTableWidgetItem *item0=new QTableWidgetItem;
    item0->setText(name);
    //int nbrow=model->rowCount();
    //model->setItem(row, 0, item0);
    ui->tableView->setItem(row, 0, item0);
    QTableWidgetItem *item1=new QTableWidgetItem;
    item1->setText(cmd);
    //model->setItem(row, 1, item1);
    ui->tableView->setItem(row, 1, item1);
    QTableWidgetItem *item2=new QTableWidgetItem;
    item2->setText(type);
    //model->setItem(row, 2, item2);
    ui->tableView->setItem(row, 2, item2);
    //if(model->rowCount()>0){
    if(ui->tableView->rowCount()>0){
        ui->pushButtonEdit->setEnabled(true);
    }else{
        ui->pushButtonEdit->setEnabled(false);
    }*/
}

void MainWindow::on_pushButtonAdd_clicked()
{
    QTreeWidgetItem *item = new QTreeWidgetItem();
    settingcmdwindow->itemEdited=item;
    settingcmdwindow->isEditMode=false;
    settingcmdwindow->show();
    settingcmdwindow->raise();
    settingcmdwindow->activateWindow();
    settingcmdwindow->move(QCursor::pos());

}

void MainWindow::on_pushButtonRemove_clicked()
{
    QList<QTreeWidgetItem *> selectedFiles= ui->tableView->selectedItems();
    if(selectedFiles.size())
    {
        foreach(QTreeWidgetItem *i, selectedFiles)
        {
            Logger::log(Logger::DEBUG,Q_FUNC_INFO,"data in tree removed (name="+i->data(0,Qt::DisplayRole).toString()+")");
            delete i;
        }
    }
    if(ui->tableView->topLevelItemCount()>0){
        ui->pushButtonEdit->setEnabled(true);
    }else{
        ui->pushButtonEdit->setEnabled(false);
    }
}

void MainWindow::on_pushButtonEdit_clicked()
{
    QTreeWidgetItem *toEdit = ui->tableView->currentItem();
    if(toEdit->flags()==(Qt::ItemIsSelectable| Qt::ItemIsEnabled | Qt::ItemIsDragEnabled)){
        settingcmdwindow->setItemToEdit(toEdit);
        settingcmdwindow->show();
        settingcmdwindow->activateWindow();
        settingcmdwindow->move(QCursor::pos());
    }
    /*   int row=ui->tableView->currentIndex().row();
    if(row>=0){
        Logger::log(Logger::DEBUG,Q_FUNC_INFO,"data in tree edited (ligne="+QString::number(row)+")");
        //QStandardItem *item0 = model->item(row,0);
        QTableWidgetItem *item0 = ui->tableView->item(row,0);
        settingcmdwindow->setName(item0->text());
        //QStandardItem *item1 = model->item(row,1);
        QTableWidgetItem *item1 = ui->tableView->item(row,1);
        settingcmdwindow->setCMD(item1->text());
        //QStandardItem *item2 = model->item(row,2);
        QTableWidgetItem *item2 = ui->tableView->item(row,2);
        settingcmdwindow->setType(item2->text());
        settingcmdwindow->isEditMode=true;
        settingcmdwindow->show();
        settingcmdwindow->activateWindow();
        settingcmdwindow->move(QCursor::pos());
        settingcmdwindow->setWindowOpacity(0.8);
    }*/
}

void MainWindow::on_tableView_doubleClicked(const QModelIndex &index)
{
    if(!toSendProcess->isRunning()){
        toSendProcess->howToRead=Process::READNOTDEFINED;
        toSendProcess->start();
    }
}

void MainWindow::onPortAddedOrRemoved()
{
    Logger::log(Logger::DEBUG,Q_FUNC_INFO,"Port have been discovered or lost");
    refreshButtonTimer->start();
    QString current = ui->comboBoxComPort->currentText();

    ui->comboBoxComPort->blockSignals(true);
    ui->comboBoxComPort->clear();
    foreach (const QextPortInfo info, enumerator->getPorts()) {
        PortSettings settings = {BAUD9600, DATA_8, PAR_NONE, STOP_1, FLOW_OFF, 10};
        QextSerialPort serial(info.portName, settings, QextSerialPort::Polling);
        //  if (serial.open(QIODevice::ReadWrite)){
        ui->comboBoxComPort->addItem(info.portName);
        //      serial.close();
        //  }
    }
    ui->comboBoxComPort->setCurrentIndex(ui->comboBoxComPort->findText(current));
    ui->comboBoxComPort->blockSignals(false);
}

void MainWindow::onPortNameChanged(const QString &name)
{
    qDebug()<<name;

}
//! [2]
void MainWindow::onBaudRateChanged(int idx)
{
    port->setBaudRate((BaudRateType)ui->comboBoxBaudRate->itemData(idx).toInt());
    settingsRegister->setValue(settingGroup+"/"+saveBaudRate,ui->comboBoxBaudRate->currentText());
}

void MainWindow::onParityChanged(int idx)
{
    port->setParity((ParityType)ui->comboBoxParity->itemData(idx).toInt());
    settingsRegister->setValue(settingGroup+"/"+saveParity,ui->comboBoxParity->currentText());
}

void MainWindow::onDataBitsChanged(int idx)
{
    port->setDataBits((DataBitsType)ui->comboBoxDataBits->itemData(idx).toInt());
    settingsRegister->setValue(settingGroup+"/"+saveDataBits,ui->comboBoxDataBits->currentText());
}

void MainWindow::onStopBitsChanged(int idx)
{
    port->setStopBits((StopBitsType)ui->comboBoxStopBits->itemData(idx).toInt());
    settingsRegister->setValue(settingGroup+"/"+saveStopBits,ui->comboBoxStopBits->currentText());
}

void MainWindow::on_comboBoxFlowControl_currentIndexChanged(int index)
{
    port->setFlowControl((FlowType)ui->comboBoxFlowControl->itemData(index).toInt());
    settingsRegister->setValue(settingGroup+"/"+saveFlowControl,ui->comboBoxFlowControl->currentText());
}

void MainWindow::buttonGroupStateChange(int index)
{
    settingsRegister->setValue(settingGroup+"/"+saveDataFormat,index);
}

void MainWindow::buttonGroup_2StateChange(int index)
{
    settingsRegister->setValue(settingGroup+"/"+saveNewLineSettings,index);
}

void MainWindow::onQueryModeChanged(int idx)
{
    switch(ui->comboBoxQueryMode->itemData(idx).toInt()){
    case QextSerialPort::Polling:
        ui->spinBoxTimeout->setEnabled(true);
        ui->labelTimeout->setEnabled(true);
        timer->start();
        break;
    case QextSerialPort::EventDriven:
        ui->spinBoxTimeout->setEnabled(false);
        ui->labelTimeout->setEnabled(false);
        timer->stop();
        break;
    }

    port->setQueryMode((QextSerialPort::QueryMode)ui->comboBoxQueryMode->itemData(idx).toInt());
    settingsRegister->setValue(settingGroup+"/"+saveQueryMode,ui->comboBoxQueryMode->currentText());
    if(port->isOpen())
        onOpenCloseButtonClicked();
}
void MainWindow::onOpenCloseButtonClicked()
{
    if (!port->isOpen()) {
        port->setPortName(ui->comboBoxComPort->currentText());
        if(port->open(QIODevice::ReadWrite)){
            // QObject::connect(port, SIGNAL(readyRead()),this,SLOT(onReadyRead()));
            Logger::log(Logger::DEBUG,Q_FUNC_INFO,"Port name="+port->portName()+" baudrate ="+QString::number(port->baudRate()));
            ui->labelConectedLed->setPixmap(QPixmap(QString::fromUtf8(":/images/images/circle_green.png")));
            ui->pushButtonConnect->setEnabled(false);
            ui->pushButtonDisconnect->setEnabled(true);
        }else{
            ui->labelConectedLed->setPixmap(QPixmap(QString::fromUtf8(":/images/images/circle_red.png")));
            ui->pushButtonConnect->setEnabled(true);
            ui->pushButtonDisconnect->setEnabled(false);
        }
    }
    else {
        port->close();
        ui->labelConectedLed->setPixmap(QPixmap(QString::fromUtf8(":/images/images/circle_red.png")));
        ui->pushButtonConnect->setEnabled(true);
        ui->pushButtonDisconnect->setEnabled(false);
    }

    //If using polling mode, we need a QTimer
    if (port->isOpen() && port->queryMode() == QextSerialPort::Polling)
        timer->start();
    else
        timer->stop();

    //update led's status

}

void MainWindow::onSendButtonClicked()
{
    Logger::log(Logger::DEBUG,Q_FUNC_INFO,"click on send button : "+ui->lineEditToSend->text());
    sendText(ui->lineEditToSend->text());
}
int numberRx=0;
void MainWindow::onReadyRead()
{
    QString dataToplot;
    if (port->bytesAvailable()) {
        numberRx+=port->bytesAvailable()*8;
        toggleurRx->setInterval(1);
        if(!toggleurRx->isActive()){

            //int sizeData=tosend.size()*8;

            toggleurRx->start();
        }
        Logger::log(Logger::DEBUG,Q_FUNC_INFO,"nb de byte read = "+QString::number(port->bytesAvailable()));
        ui->plainTextEditRecieved->moveCursor(QTextCursor::End);
        QString dataRead =QString(port->readAll());

        if(ui->radioButtonBIN->isChecked()){
            dataRead=convert::asciitoBin(dataRead,"0b"," ");
        }else if(ui->radioButtonDEC->isChecked()){
            dataRead=convert::asciitoDecimal(dataRead,""," ");
        }else if(ui->radioButtonHEX->isChecked()){
            dataRead=convert::asciitoHex(dataRead,"0x"," ");
        }
        dataToplot=dataRead;

        Logger::log(Logger::DEBUG,Q_FUNC_INFO,"data read ="+dataRead);
        if(ui->checkBoxTimeLine->isChecked() && ui->checkBoxTimeLine->isEnabled()){
            //dataToplot = dataToplot.simplified();
            dataToplot.prepend("["+QDateTime::currentDateTime().toString("dd/MM/yy hh:mm:ss,zzz")+"] ").append("\r\n");
        }

        dataToplot.replace("\r\n","<BR>");
        dataToplot.replace("\r","<BR>");
        dataToplot.replace("\n","<BR>");
        dataToplot.replace(" ","&nbsp;");

        dataToplot.prepend("<FONT COLOR=\""+pref->couleurReceive.name()+"\">");
        dataToplot.append("</font>");
        // ui->plainTextEditRecieved->insertPlainText(dataToplot);
        Logger::log(Logger::DEBUG,Q_FUNC_INFO,"data plot ="+dataToplot);

        if(pref->sendReceiveSameBox){
            ui->plainTextEditSend->moveCursor(QTextCursor::End);
            ui->plainTextEditSend->insertHtml(dataToplot);
        }else{
            ui->plainTextEditRecieved->moveCursor(QTextCursor::End);
            ui->plainTextEditRecieved->insertHtml(dataToplot);
        }
    }

}

void MainWindow::on_actionNew_triggered()
{

}

void MainWindow::on_spinBoxTimeout_valueChanged(int arg1)
{
    timer->setInterval(arg1);
    settingsRegister->setValue(settingGroup+"/"+saveTimeout,ui->spinBoxTimeout->value());
}

void MainWindow::on_pushButtonClearSend_clicked()
{
    ui->plainTextEditSend->clear();
}

void MainWindow::on_pushButtonClearReceived_clicked()
{
    ui->plainTextEditRecieved->clear();
}

void MainWindow::toggleLED(QLabel *led){
    //QTimer *toggleur =new QTimer(this);
    //toggleur->setInterval(10);
    //QObject::connect(toggleur,SIGNAL(timeout(QPrivateSignal),this(SLOT())));
    led->setPixmap(QPixmap(QString::fromUtf8(":/images/images/circle_red.png")));
    led->setPixmap(QPixmap(QString::fromUtf8(":/images/images/circle_green.png")));
}

void MainWindow::getAllSettings()
{
    QString stringReponse = "";
    int intReponse =0;
    bool boolReponse=false;

    stringReponse=settingsRegister->value(settingGroup+"/"+savePortCOM,"").toString();
    ui->comboBoxComPort->setCurrentText(stringReponse);
    on_comboBoxComPort_currentIndexChanged(stringReponse);

    stringReponse=settingsRegister->value(settingGroup+"/"+saveBaudRate,"9600").toString();
    ui->comboBoxBaudRate->setCurrentText(stringReponse);
    onBaudRateChanged(ui->comboBoxBaudRate->currentIndex());

    stringReponse=settingsRegister->value(settingGroup+"/"+saveStopBits,"1").toString();
    ui->comboBoxStopBits->setCurrentText(stringReponse);
    onStopBitsChanged(ui->comboBoxStopBits->currentIndex());

    stringReponse=settingsRegister->value(settingGroup+"/"+saveParity,"NONE").toString();
    ui->comboBoxParity->setCurrentText(stringReponse);
    onParityChanged(ui->comboBoxParity->currentIndex());

    stringReponse=settingsRegister->value(settingGroup+"/"+saveDataBits,"8").toString();
    ui->comboBoxDataBits->setCurrentText(stringReponse);
    onDataBitsChanged(ui->comboBoxDataBits->currentIndex());

    stringReponse=settingsRegister->value(settingGroup+"/"+saveQueryMode,"Synchronously").toString();
    ui->comboBoxQueryMode->setCurrentText(stringReponse);
    onQueryModeChanged(ui->comboBoxQueryMode->currentIndex());

    intReponse=settingsRegister->value(settingGroup+"/"+saveTimeout,"100").toInt();
    ui->spinBoxTimeout->setValue(intReponse);
    on_spinBoxTimeout_valueChanged(intReponse);

    stringReponse=settingsRegister->value(settingGroup+"/"+saveFlowControl,"FLOW OFF").toString();
    ui->comboBoxFlowControl->setCurrentText(stringReponse);
    on_comboBoxFlowControl_currentIndexChanged(ui->comboBoxFlowControl->currentIndex());

    intReponse=settingsRegister->value(settingGroup+"/"+saveDataFormat,"-2").toInt();
    ui->buttonGroup->button(intReponse)->setChecked(true);

    intReponse=settingsRegister->value(settingGroup+"/"+saveNewLineSettings,"-5").toInt();
    ui->buttonGroup_2->button(intReponse)->setChecked(true);

    stringReponse=settingsRegister->value(settingGroup+"/"+saveNewLineSettingsCustom,";").toString();
    ui->lineEditCustomEndLine->setText(stringReponse);

    boolReponse=settingsRegister->value(settingGroup+"/"+saveEraseAfterSend,"true").toBool();
    ui->checkBoxEraseAfterSend->setChecked(boolReponse);

    boolReponse=settingsRegister->value(settingGroup+"/"+saveTimeLine,"true").toBool();
    ui->checkBoxTimeLine->setChecked(boolReponse);

    boolReponse=settingsRegister->value(settingGroup+"/"+saveAppendNewLine,"true").toBool();
    ui->checkBoxAppendNewLine->setChecked(boolReponse);

    boolReponse=settingsRegister->value(settingGroup+"/"+saveEnvoyerEnBoucle,"false").toBool();
    ui->checkBoxWhile->setChecked(boolReponse);

    intReponse=settingsRegister->value(settingGroup+"/"+saveEnvoyerEnBoucleTime,"1000").toInt();
    ui->spinBoxTime->setValue(intReponse);

    boolReponse=settingsRegister->value(settingGroup+"/"+saveSendAsTyping,"false").toBool();
    ui->checkBoxSendAsTyping->setChecked(boolReponse);
}

bool stateToggleRX=false;
void MainWindow::timerToggleRxd(){
    //Logger::log(Logger::DEBUG,Q_FUNC_INFO,"intervale RX : "+QString::number(toggleurRx->interval()));
    if(stateToggleRX)
        ui->labelRxDLed->setPixmap(QPixmap(QString::fromUtf8(":/images/images/circle_red.png")));
    else
        ui->labelRxDLed->setPixmap(QPixmap(QString::fromUtf8(":/images/images/circle_green.png")));
    stateToggleRX=!stateToggleRX;
    int baudms=ui->comboBoxBaudRate->itemData(ui->comboBoxBaudRate->currentIndex()).toInt()/1000;
    if((numberRx-=baudms)<0){
        numberRx=0;
        toggleurRx->stop();
        ui->labelRxDLed->setPixmap(QPixmap(QString::fromUtf8(":/images/images/circle_red.png")));
    }
}
bool stateToggleTX=false;
void MainWindow::timerToggleTxd(){
    if(stateToggleTX)
        ui->labelTxDLed->setPixmap(QPixmap(QString::fromUtf8(":/images/images/circle_red.png")));
    else
        ui->labelTxDLed->setPixmap(QPixmap(QString::fromUtf8(":/images/images/circle_green.png")));
    stateToggleTX=!stateToggleTX;
    int baudms=ui->comboBoxBaudRate->itemData(ui->comboBoxBaudRate->currentIndex()).toInt()/1000;
    if((numberTx-=baudms)<0){
        numberTx=0;
        toggleurTx->stop();
        ui->labelTxDLed->setPixmap(QPixmap(QString::fromUtf8(":/images/images/circle_red.png")));
    }
}

void MainWindow::on_lineEditToSend_returnPressed()
{
    ui->pushButtonSend->click();
}

void MainWindow::on_comboBoxComPort_currentIndexChanged(const QString &arg1)
{
    Logger::log(Logger::DEBUG,Q_FUNC_INFO,"New Port name : "+arg1);
    settingsRegister->setValue(settingGroup+"/"+savePortCOM,arg1);
    if(port->isOpen()){
        port->close();
        ui->labelConectedLed->setPixmap(QPixmap(QString::fromUtf8(":/images/images/circle_red.png")));
        ui->pushButtonConnect->setEnabled(true);
        ui->pushButtonDisconnect->setEnabled(false);
    }
}

void MainWindow::on_pushButtonAddFolder_clicked()
{
    Logger::log(Logger::DEBUG,Q_FUNC_INFO,"Folder in tree added");
    QStringList list;
    list<<"Folder";
    QTreeWidgetItem *item;
    if(ui->tableView->currentItem()!=NULL){
        if(ui->tableView->currentItem()->flags()==(Qt::ItemIsSelectable| Qt::ItemIsEnabled | Qt::ItemIsDragEnabled)){
            item = new QTreeWidgetItem(ui->tableView, list);
        }else{
            item = new QTreeWidgetItem(ui->tableView->currentItem(), list);
        }
    }else{
        item = new QTreeWidgetItem(ui->tableView, list);
    }
    item->setIcon(0,QPixmap(QString::fromUtf8(":/images/images/folder.png")));
    item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsUserCheckable | Qt::ItemIsEnabled | Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled | Qt::ItemIsEditable);
}

void MainWindow::on_pushButton_clicked()
{
    if(!toSendProcess->isRunning()){
        toSendProcess->howToRead=Process::READALL;
        toSendProcess->start();
    }
}

void MainWindow::sendText()
{
    sendText(pastDataSend);
}

void MainWindow::on_spinBoxInterCMD_valueChanged(int arg1)
{
    toSendProcess->timeIntervall=arg1;
}

void MainWindow::on_pushButtonMask_clicked()
{
    ui->frame->setVisible(!ui->frame->isVisible());
}
int numberOfrotate=360;
void MainWindow::refreshingbutton()
{
    QTransform t;
    t.rotate(numberOfrotate-=10);
    ui->pushButtonReloadPort->setIcon(QPixmap(QString::fromUtf8(":/images/images/reload.png")).transformed(t));
    if(numberOfrotate<=0){
        refreshButtonTimer->stop();
        numberOfrotate=360;
    }
}

void MainWindow::on_actionOuvrir_une_configuration_triggered()
{
    actualPathForSave=XmlDom::XmlDomGetxml(ui->tableView);
    if(ui->tableView->topLevelItemCount()>0)
        ui->pushButtonEdit->setEnabled(true);
    else
        ui->pushButtonEdit->setEnabled(false);
}

void MainWindow::on_actionSave_configuration_triggered()
{
    actualPathForSave=XmlDom::XmlDomSetxml(ui->tableView,actualPathForSave);
}

void MainWindow::on_actionNouvelle_configuration_triggered()
{
    int reponse = QMessageBox::warning(this,"Warning","La configuration actuel va etre effacée.\n pensez a sauvegarder cette configuration avant de continuer !\n voulez-vous continuer ?",QMessageBox::Ok,QMessageBox::Cancel);
    if(reponse== QMessageBox::Ok){
        ui->tableView->clear();
        actualPathForSave="";
    }
    if(ui->tableView->topLevelItemCount()>0)
        ui->pushButtonEdit->setEnabled(true);
    else
        ui->pushButtonEdit->setEnabled(false);
}

void MainWindow::on_actionSave_as_Configuration_triggered()
{
    actualPathForSave=XmlDom::XmlDomSetxml(ui->tableView,"");
}

void MainWindow::on_actionNouvelle_commande_triggered()
{
    on_pushButtonAdd_clicked();
}

void MainWindow::on_actionNouveau_fichier_triggered()
{
    on_pushButtonAddFolder_clicked();
}

void MainWindow::on_actionTable_ASCII_triggered()
{
    ASCIIChart *form = new ASCIIChart();
    form->show();
    form->raise();
    form->activateWindow();
}

void MainWindow::triggeredHistorique(QAction *action)
{
    ui->lineEditToSend->clear();
    ui->lineEditToSend->setText(action->text());
}

void MainWindow::on_actionPr_f_rences_triggered()
{
    pref->show();
    pref->raise();
    pref->activateWindow();
}

void MainWindow::pref_ShowStatusChange(bool arg1)
{
    ui->groupBoxStatus->setVisible(arg1);
}

void MainWindow::pref_ShowExtendChange(bool arg1)
{
    ui->labelStopBits->setVisible(arg1);
    ui->comboBoxStopBits->setVisible(arg1);
    ui->labelParity->setVisible(arg1);
    ui->comboBoxParity->setVisible(arg1);
    ui->labelDatabits->setVisible(arg1);
    ui->comboBoxDataBits->setVisible(arg1);
    ui->labelQueryMode->setVisible(arg1);
    ui->comboBoxQueryMode->setVisible(arg1);
    ui->labelTimeout->setVisible(arg1);
    ui->spinBoxTimeout->setVisible(arg1);
    ui->labelFlowControl->setVisible(arg1);
    ui->comboBoxFlowControl->setVisible(arg1);
}

void MainWindow::pref_ShowCMDTableChange(bool arg1)
{
    ui->frame_2->setVisible(arg1);
}

void MainWindow::pref_SplitRecepAndTransChange(bool arg1)
{
    ui->plainTextEditRecieved->setVisible(arg1);
    ui->labelReceived->setVisible(arg1);
    ui->pushButtonClearReceived->setVisible(arg1);
    if(arg1)
        ui->label->setText("Emission :");
    else
        ui->label->setText("Emission et Reception :");
}

void MainWindow::on_actionAbout_triggered()
{
    aboutScreen->show();
    aboutScreen->raise();
    aboutScreen->activateWindow();
}

void MainWindow::on_pushButtonDTR_clicked()
{
    DTRState=!DTRState;
    port->setDtr(DTRState);

    if(!DTRState)
        ui->pushButtonDTR->setIcon(QPixmap(QString::fromUtf8(":/images/images/circle_red.png")));
    else
        ui->pushButtonDTR->setIcon(QPixmap(QString::fromUtf8(":/images/images/circle_green.png")));
}

void MainWindow::on_pushButtonRTS_clicked()
{
    RTSState=!RTSState;
    port->setRts(RTSState);

    if(!RTSState)
        ui->pushButtonRTS->setIcon(QPixmap(QString::fromUtf8(":/images/images/circle_red.png")));
    else
        ui->pushButtonRTS->setIcon(QPixmap(QString::fromUtf8(":/images/images/circle_green.png")));
}

void MainWindow::on_checkBoxWhile_toggled(bool checked)
{
    if(!checked){
        sendTextWhile->stop();
    }

}

void MainWindow::on_spinBoxTime_valueChanged(int arg1)
{
    sendTextWhile->setInterval(arg1);
    settingsRegister->setValue(settingGroup+"/"+saveEnvoyerEnBoucleTime,arg1);
}

void MainWindow::on_toolButtonFile_clicked()
{
    QString path = QFileDialog::getOpenFileName(this,"file to send ...",QDir::homePath());
    QFile fileTosend(path);
    if(fileTosend.open(QFile::ReadOnly)){
        QTextStream stream(&fileTosend);
        sendText(stream.readAll());
    }
    fileTosend.close();
}

void MainWindow::colorTextChange(QColor newcolorSend, QColor oldcolorSend, QColor newColorReceived, QColor oldcolorReceived)
{
    QString toReplaceRecieved =ui->plainTextEditRecieved->toHtml();
    toReplaceRecieved.replace(oldcolorReceived.name(),newColorReceived.name());
    ui->plainTextEditRecieved->setHtml(toReplaceRecieved);

    QString toReplaceSend =ui->plainTextEditSend->toHtml();
    toReplaceSend.replace(oldcolorSend.name(),newcolorSend.name());
    if(pref->sendReceiveSameBox)
        toReplaceSend.replace(oldcolorReceived.name(),newColorReceived.name());
    ui->plainTextEditSend->setHtml(toReplaceSend);

}

void MainWindow::on_checkBoxSendAsTyping_toggled(bool checked)
{
    ui->pushButtonSend->setEnabled(!checked);
    ui->checkBoxEraseAfterSend->setEnabled(!checked);
    ui->checkBoxEraseAfterSend->setChecked(checked);
    ui->checkBoxAppendNewLine->setEnabled(!checked);
    ui->checkBoxTimeLine->setEnabled(!checked);
    ui->checkBoxWhile->setEnabled(!checked);
    ui->spinBoxTime->setEnabled(!checked);
    if(checked){
        QObject::connect(ui->lineEditToSend,SIGNAL(textEdited(QString)),this,SLOT(sendText(QString)));
        QObject::connect(ui->lineEditToSend,SIGNAL(returnPressed()),this,SLOT(sendTextReturn()));
    }else{
        QObject::disconnect(ui->lineEditToSend,SIGNAL(textEdited(QString)),this,SLOT(sendText(QString)));
        QObject::disconnect(ui->lineEditToSend,SIGNAL(returnPressed()),this,SLOT(sendTextReturn()));
    }
    settingsRegister->setValue(settingGroup+"/"+saveSendAsTyping,checked);
}

void MainWindow::exitApp()
{
    qApp->quit();
}

void MainWindow::reboot()
{
    qApp->quit();

#ifdef Q_OS_WIN
    int result = (int)::ShellExecuteA(0, "open", QApplication::applicationFilePath().toUtf8().constData(), 0, 0, SW_SHOWNORMAL);
    if (SE_ERR_ACCESSDENIED == result)
    {
        // Requesting elevation
        result = (int)::ShellExecuteA(0, "runas", QApplication::applicationFilePath().toUtf8().constData(), 0, 0, SW_SHOWNORMAL);
    }
    if (result <= 32)
    {
        // error handling
    }
#else
    QProcess::startDetached(qApp->arguments()[0], qApp->arguments());
#endif
}

void MainWindow::on_pushButtonConnect_clicked()
{

}



void MainWindow::on_lineEditCustomEndLine_editingFinished()
{
    settingsRegister->setValue(settingGroup+"/"+saveNewLineSettingsCustom,ui->lineEditCustomEndLine->text());
}

void MainWindow::on_checkBoxEraseAfterSend_toggled(bool checked)
{
    settingsRegister->setValue(settingGroup+"/"+saveEraseAfterSend,checked);
}

void MainWindow::on_checkBoxTimeLine_toggled(bool checked)
{
    settingsRegister->setValue(settingGroup+"/"+saveTimeLine,checked);
}

void MainWindow::on_checkBoxAppendNewLine_toggled(bool checked)
{
    settingsRegister->setValue(settingGroup+"/"+saveAppendNewLine,checked);
}

void MainWindow::on_spinBoxDecimal_valueChanged(int arg1)
{
    ui->spinBoxDecimal->blockSignals(true);
    ui->lineEditcharacter->blockSignals(true);
    hexspinbox->blockSignals(true);
    binspinbox->blockSignals(true);
    bool ok=false;
    binspinbox->setValue(arg1);
    hexspinbox->setValue(arg1);
    ui->lineEditcharacter->setText(convert::decimaltoAscii(&ok,QString::number(arg1,10)));

    ui->spinBoxDecimal->blockSignals(false);
    ui->lineEditcharacter->blockSignals(false);
    hexspinbox->blockSignals(false);
    binspinbox->blockSignals(false);
}

void MainWindow::hexBoxDecimal_valueChanged(int arg1)
{
    ui->spinBoxDecimal->blockSignals(true);
    ui->lineEditcharacter->blockSignals(true);
    hexspinbox->blockSignals(true);
    binspinbox->blockSignals(true);
    bool ok=false;
    ui->spinBoxDecimal->setValue(arg1);
    binspinbox->setValue(arg1);
    ui->lineEditcharacter->setText(convert::decimaltoAscii(&ok,QString::number(arg1,10)));

    ui->spinBoxDecimal->blockSignals(false);
    ui->lineEditcharacter->blockSignals(false);
    hexspinbox->blockSignals(false);
    binspinbox->blockSignals(false);
}

void MainWindow::binBoxDecimal_valueChanged(int arg1)
{
    ui->spinBoxDecimal->blockSignals(true);
    ui->lineEditcharacter->blockSignals(true);
    hexspinbox->blockSignals(true);
    binspinbox->blockSignals(true);
    bool ok=false;
    ui->spinBoxDecimal->setValue(arg1);
    hexspinbox->setValue(arg1);
    ui->lineEditcharacter->setText(convert::decimaltoAscii(&ok,QString::number(arg1,10)));

    ui->spinBoxDecimal->blockSignals(false);
    ui->lineEditcharacter->blockSignals(false);
    hexspinbox->blockSignals(false);
    binspinbox->blockSignals(false);
}

void MainWindow::on_lineEditcharacter_textChanged(const QString &arg1)
{
    int value = convert::asciitoDecimal(arg1).toInt(NULL,10);
    ui->spinBoxDecimal->blockSignals(true);
    ui->lineEditcharacter->blockSignals(true);
    hexspinbox->blockSignals(true);
    binspinbox->blockSignals(true);
    bool ok=false;
    ui->spinBoxDecimal->setValue(value);
    hexspinbox->setValue(value);
    binspinbox->setValue(value);

    ui->spinBoxDecimal->blockSignals(false);
    ui->lineEditcharacter->blockSignals(false);
    hexspinbox->blockSignals(false);
    binspinbox->blockSignals(false);
}
