#ifndef XMLDOM_H
#define XMLDOM_H

#include <QtXml>
#include <QTreeWidget>

class XmlDom
{
public:
    XmlDom();
    static void processItem(QDomElement parent, QTreeWidgetItem *treeparent);
   // static QList<QTreeWidgetItem *> items;
    //QTreeWidget *treeWidget;
    static QString XmlDomGetxml(QTreeWidget *treeWidget);
    static QString XmlDomSetxml(QTreeWidget *treeWidget, QString pathWhereSave);
    static void setProcessItem(QTreeWidgetItem *parent, QDomElement *xmlparent, QDomDocument *dom);
};

#endif // XMLDOM_H
