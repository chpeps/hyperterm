#include "processSending.h"
#include "mainwindow.h"
#include "logger.h"

Process::Process(QObject* parent, QextSerialPort *port, QTreeWidget *treeWidget, int TimeIntervall, int howToRead):
    QThread(parent)
{
    Logger::log(Logger::DEBUG,Q_FUNC_INFO,"Start init Process");
    if(parent==NULL)
        moveToThread(this);

    //si le port est null on exit le process
    if(port==NULL){
        exit(-1);
    }else{
        this->port=port;
    }

    if(treeWidget==NULL){
        exit(-1);
    }else{
        this->treeWidget=treeWidget;
    }

    //si la valeur TimeInterVall est egal a 0 alors on regle le delay par defaut de 1 ms
    if(TimeIntervall==0)
        TimeIntervall=1;
    this->timeIntervall=TimeIntervall;

    Progression=NULL;
    this->howToRead=howToRead;
    listTosend.clear();
    Logger::log(Logger::DEBUG,Q_FUNC_INFO,"End init Process");
}

Process::~Process()
{
    Logger::log(Logger::DEBUG,Q_FUNC_INFO," Process Stop");
    exit();
    wait();
    // nettoyage

}

void Process::run()
{
    Logger::log(Logger::DEBUG,Q_FUNC_INFO,"Start running Process");
    int row=treeWidget->currentIndex().row();
    QString name="";
    QString cmd="";
    int time =1;
    QString type="";
    if(row>=0 || howToRead==READALL){
        QTreeWidgetItem *parent =treeWidget->currentItem();
        if(howToRead==READNOTDEFINED){
            if(parent->flags()==(Qt::ItemIsSelectable| Qt::ItemIsEnabled | Qt::ItemIsDragEnabled)){
                howToRead=READONE;
            }else{
                howToRead=READCHILDREN;
            }
        }
        switch (howToRead) {
        case READALL:
            Logger::log(Logger::DEBUG,Q_FUNC_INFO,"READALL");
            for (int i = 0; i < treeWidget->topLevelItemCount(); ++i) {
                QTreeWidgetItem *item = treeWidget->topLevelItem(i);
                if(item->data(0,Qt::DisplayRole).toString().at(0)!='#'){
                    if(item->flags()==(Qt::ItemIsSelectable| Qt::ItemIsEnabled | Qt::ItemIsDragEnabled))
                        listTosend.append(item);
                    else
                        processItem(item);
                }
            }
            break;
        case READCHILDREN:
            Logger::log(Logger::DEBUG,Q_FUNC_INFO,"READCHILDREN");
            for (int i = 0; i < parent->childCount(); ++i) {
                QTreeWidgetItem *item = parent->child(i);
                if(item->data(0,Qt::DisplayRole).toString().at(0)!='#'){
                    if(item->flags()==(Qt::ItemIsSelectable| Qt::ItemIsEnabled | Qt::ItemIsDragEnabled))
                        listTosend.append(item);
                    else
                        processItem(item);
                }
            }
            break;
        default:
        case READONE:
            Logger::log(Logger::DEBUG,Q_FUNC_INFO,"READONE");
            if(parent->flags()==(Qt::ItemIsSelectable| Qt::ItemIsEnabled | Qt::ItemIsDragEnabled)){
                listTosend.append(treeWidget->currentItem());
            }
            break;
        }
        if(listTosend.size()>0)
            emit setMaximum(listTosend.size());
        int actual =1;
        foreach (QTreeWidgetItem *item, listTosend) {
            name=item->data(0,Qt::DisplayRole).toString();
            cmd=item->data(1,Qt::DisplayRole).toString();
            time=item->data(2,Qt::DisplayRole).toInt();
            type=item->data(3,Qt::DisplayRole).toString();
            Logger::log(Logger::DEBUG,Q_FUNC_INFO,"data in tree wil be send (name="+name+", CMD="+cmd+", type="+type+")");
            emit toSendText(cmd);
            if(listTosend.size()>actual){
                Logger::log(Logger::DEBUG,Q_FUNC_INFO,"Wait "+QString::number(time)+"ms");
                this->msleep(time);
            }
            emit setValue(actual++);
        }

        listTosend.clear();
    }

    Logger::log(Logger::DEBUG,Q_FUNC_INFO,"Stop running Process");
}


void Process::processItem(QTreeWidgetItem *parent)
{
    for (int i = 0; i < parent->childCount(); ++i) {
        QTreeWidgetItem *child = parent->child(i);
        if(child->data(0,Qt::DisplayRole).toString().at(0)!='#'){
            if(child->flags()==(Qt::ItemIsSelectable| Qt::ItemIsEnabled | Qt::ItemIsDragEnabled))
                listTosend.append(child);
            else
                processItem(child);
        }
    }
}
