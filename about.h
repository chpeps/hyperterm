#ifndef ABOUT_H
#define ABOUT_H

#include <QWidget>
#include <QtNetwork>

namespace Ui {
class about;
}

class about : public QWidget
{
    Q_OBJECT
    
public:
    explicit about(QWidget *parent = 0);
    ~about();

signals:
    void reboot();
    void exitApp();
private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void progressionTelechargement(qint64 bytesReceived, qint64 bytesTotal);

    void enregistrer();

private:
    Ui::about *ui;
    QNetworkReply *reply;
    QNetworkAccessManager manager;
    QWidget *parentMain;
};

#endif // ABOUT_H
