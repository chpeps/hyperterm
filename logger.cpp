#include "logger.h"
#include <QDebug>
#include <QDateTime>
#include <QFile>
#include <QFileDialog>

QFile* Logger::logFile= new QFile("logZhyperTerm.txt");
int Logger::SELECTEDLEVELLOG = DEBUG;
int Logger::SELECTEDOUTPRINT = INFILE;

Logger::Logger()
{
}

void Logger::log(int level, QString qInfoFonction, QString message)
{
    if(level>=SELECTEDLEVELLOG){
        QString toPrint=QDateTime::currentDateTime().toString("hh:mm:ss,zzz dd/MM/yy")+" ";
        if(level==DEBUG){
            toPrint+="   [DEBUG]";
        }else if(level==INFO){
            toPrint+="    [INFO]";
        }else if(level==WARNING){
            toPrint+=" [WARNING]";
        }else if(level==CRITICAL){
            toPrint+="[CRITICAL]";
        }
        toPrint+=qInfoFonction+" -> "+message;
        out(toPrint);
    }
}

void Logger::remove()
{
    if(logFile->exists())
        logFile->remove();
}

void Logger::removeLoggerFile()
{
    remove();
}

void Logger::saveAsLoggerFile()
{
    if(logFile->exists()){
        QString fn = QFileDialog::getSaveFileName(NULL, "Save as...",QString(), "Text files (*.txt)");
        if (fn.isEmpty())
            return ;
        if (!fn.endsWith(".txt", Qt::CaseInsensitive))
            fn += ".txt"; // default
        logFile->copy(fn);
    }
}

void Logger::stopFileLog(bool state)
{
    if(state){
        SELECTEDOUTPRINT=INTERM;//default
    }else{
        SELECTEDOUTPRINT=INFILE;
    }
}

void Logger::out(QString toPrint)
{
    // QFile *logFile;
    switch(SELECTEDOUTPRINT){
    case INFILE :
        // logFile = new QFile("log.txt");
        if(logFile->open(QIODevice::WriteOnly| QIODevice::Text|QIODevice::Append)){
            QTextStream out(logFile);
            out << toPrint<<"\r\n";
            logFile->close();
        }
    case INTERM :
        qDebug()<<toPrint;
        break;
    case INWEB :
        break;
    case NONE :
        break;
    }
}
