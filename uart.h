#ifndef UART_H
#define UART_H
#include <QComboBox>
#include "qextserialport.h"
#include "qextserialenumerator.h"

class uart
{
public:
    uart();
    void setComboboxBaudrate(QComboBox *comboboxBaudrate);
    void setComboboxStopBits(QComboBox *comboboxStopBits);
    void setComboboxParity(QComboBox *comboboxparity);
    void setComboboxDatabits(QComboBox *comboboxDatabits);
    void setComboboxQueryMode(QComboBox *comboboxQueryMode);
    void setComboboxFlowControl(QComboBox *comboboxFlowControl);
};

#endif // UART_H
