#ifndef ASCIICHART_H
#define ASCIICHART_H

#include <QDialog>
#include <QLabel>

namespace Ui {
class ASCIIChart;
}

class ASCIIChart : public QDialog
{
    Q_OBJECT
    
public:
    explicit ASCIIChart(QWidget *parent = 0);
    ~ASCIIChart();
    
private slots:
    void on_tableWidget_cellClicked(int row, int column);

private:
    Ui::ASCIIChart *ui;
    QLabel newlabel;
};

#endif // ASCIICHART_H
