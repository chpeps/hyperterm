#include "uart.h"
#include <QVariant>
#include "logger.h"
#include <QtCore/QList>

uart::uart()
{
}

void uart::setComboboxBaudrate(QComboBox *comboboxBaudrate)
{
    comboboxBaudrate->blockSignals(true);
#ifdef __unix__
    Logger::log(Logger::INFO,Q_FUNC_INFO,"Populate with unix Baudrate");
#endif
#ifdef _WIN32
    Logger::log(Logger::INFO,Q_FUNC_INFO,"Populate with Windows Baudrate");
#endif
#ifdef __unix__
    comboboxBaudrate->addItem("50",BAUD50);
    comboboxBaudrate->addItem("75",BAUD75);
#endif
    comboboxBaudrate->addItem("110",BAUD110);
#ifdef __unix__
    comboboxBaudrate->addItem("134.5",BAUD134);
    comboboxBaudrate->addItem("150",BAUD150);
    comboboxBaudrate->addItem("200",BAUD200);
#endif
    comboboxBaudrate->addItem("300",BAUD300);
    comboboxBaudrate->addItem("600",BAUD600);
    comboboxBaudrate->addItem("1200",BAUD1200);
#ifdef __unix__
    comboboxBaudrate->addItem("1800",BAUD1800);
#endif
    comboboxBaudrate->addItem("2400",BAUD2400);
    comboboxBaudrate->addItem("4800",BAUD4800);
    comboboxBaudrate->addItem("9600",BAUD9600);
#ifdef _WIN32
    comboboxBaudrate->addItem("14400",BAUD14400);
#endif
    comboboxBaudrate->addItem("19200",BAUD19200);
    comboboxBaudrate->addItem("38400",BAUD38400);
#ifdef _WIN32
    comboboxBaudrate->addItem("56000",BAUD56000);
#endif
    comboboxBaudrate->addItem("57600",BAUD57600);
    comboboxBaudrate->addItem("115200",BAUD115200);
#ifdef _WIN32
    comboboxBaudrate->addItem("128000",BAUD128000);
#endif
#ifdef __unix__
    comboboxBaudrate->addItem("230400",BAUD230400);
#endif
#ifdef _WIN32
    comboboxBaudrate->addItem("256000",BAUD256000);
#endif
#ifdef __unix__
    comboboxBaudrate->addItem("460800",BAUD460800);
    comboboxBaudrate->addItem("500000",BAUD500000);
    comboboxBaudrate->addItem("576000",BAUD576000);
    comboboxBaudrate->addItem("921600",BAUD921600);
    comboboxBaudrate->addItem("1000000",BAUD1000000);
    comboboxBaudrate->addItem("1152000",BAUD1152000);
    comboboxBaudrate->addItem("1500000",BAUD1500000);
    comboboxBaudrate->addItem("2000000",BAUD2000000);
    comboboxBaudrate->addItem("2500000",BAUD2500000);
    comboboxBaudrate->addItem("3000000",BAUD3000000);
    comboboxBaudrate->addItem("3500000",BAUD3500000);
    comboboxBaudrate->addItem("4000000",BAUD4000000);
#endif

    comboboxBaudrate->setCurrentText("9600");
    comboboxBaudrate->blockSignals(false);
}

void uart::setComboboxStopBits(QComboBox *comboboxStopBits)
{
    comboboxStopBits->blockSignals(true);
    comboboxStopBits->addItem("1", STOP_1);
#ifdef _WIN32
    comboboxStopBits->addItem("1.5",STOP_1_5);
#endif
    comboboxStopBits->addItem("2", STOP_2);
    comboboxStopBits->blockSignals(false);
}

void uart::setComboboxParity(QComboBox *comboboxparity)
{
    comboboxparity->blockSignals(true);
    comboboxparity->addItem("NONE", PAR_NONE);
    comboboxparity->addItem("ODD", PAR_ODD);
    comboboxparity->addItem("EVEN", PAR_EVEN);
    comboboxparity->addItem("SPACE",PAR_SPACE);
#ifdef _WIN32
    comboboxparity->addItem("MARK",PAR_MARK);
#endif
    comboboxparity->blockSignals(false);
}

void uart::setComboboxDatabits(QComboBox *comboboxDatabits)
{
    comboboxDatabits->blockSignals(true);
    comboboxDatabits->addItem("5", DATA_5);
    comboboxDatabits->addItem("6", DATA_6);
    comboboxDatabits->addItem("7", DATA_7);
    comboboxDatabits->addItem("8", DATA_8);
    comboboxDatabits->setCurrentIndex(3);
    comboboxDatabits->blockSignals(false);
}

void uart::setComboboxQueryMode(QComboBox *comboboxQueryMode)
{
    comboboxQueryMode->blockSignals(true);
    comboboxQueryMode->addItem("Asynchronously",QextSerialPort::Polling);
    comboboxQueryMode->addItem("Synchronously",QextSerialPort::EventDriven);
    comboboxQueryMode->blockSignals(false);
}

void uart::setComboboxFlowControl(QComboBox *comboboxFlowControl)
{
    comboboxFlowControl->blockSignals(true);
    comboboxFlowControl->addItem("FLOW OFF",FLOW_OFF);
    comboboxFlowControl->addItem("FLOW HARDWARE",FLOW_HARDWARE);
    comboboxFlowControl->addItem("FLOW XONXOFF",FLOW_XONXOFF);
    comboboxFlowControl->blockSignals(false);
}
