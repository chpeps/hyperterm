#include "updaterfen.h"
#include <QMessageBox>

UpdaterFen::UpdaterFen() : QWidget()
{
   //Windows Options
   setFixedSize(300,200);
   setWindowTitle("Updater");

   progression = new QProgressBar(this);
   progression->setValue(0);

#ifdef _WIN32//http://www.zokali.fr/ZhyperTerm/last/windows/HyperTerm.exe
   QString versionDirectory("windows");
   QString extension(".exe");
#elif __APPLE__
   QString versionDirectory("mac");
   QString extension(".app/Contents/MacOS/HyperTerm");
#elif __linux
    QString versionDirectory("linux");
    QString extension("");
#endif
   // http://www.zokali.fr/ZhyperTerm/last/mac/ZhyperTerm.app/Contents/MacOS/HyperTerm
    reply = manager.get(QNetworkRequest(QUrl("http://zokali.fr/ZhyperTerm/last/"+versionDirectory+"/HyperTerm"+extension)));

   connect(reply, SIGNAL(finished()), this, SLOT(enregistrer()));
   connect(reply, SIGNAL(downloadProgress(qint64, qint64)), this, SLOT(progressionTelechargement(qint64, qint64)));

}

void UpdaterFen::progressionTelechargement(qint64 bytesReceived, qint64 bytesTotal)
{
   if (bytesTotal != -1)
   {
       progression->setRange(0, bytesTotal);
       progression->setValue(bytesReceived);
   }
}

void UpdaterFen::enregistrer()
{
   reply->deleteLater();
#ifdef _WIN32
   QString extension(".exe");
#elif __APPLE__
   QString extension(".app/Contents/MacOS/HyperTerm");
#elif __linux
    QString extension("");
#endif
   QFile lastversion("HyperTerm"+extension);
  // QMessageBox::information(this, "Fin de téléchargement", "Téléchargement terminé ! ZhyperTerm"+extension);
//lastversion.remove();
   if ( lastversion.open(QIODevice::WriteOnly) )
   {
       lastversion.write(reply->readAll());
       lastversion.close();

       QMessageBox::information(this, "Fin de téléchargement", "Téléchargement terminé !");
   }else{
       QMessageBox::information(this, "Fin de téléchargement", "Probleme impossible d'ouvrir le fichier");
   }
   close();

}
