#include "preferences.h"
#include "ui_preferences.h"
#include "logger.h"
#include "QFileDialog"
#include "QFile"
#include "QColorDialog"
#include <QSettings>

/*
settings.remove("monkey");
*/

preferences::preferences(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::preferences)
{
    ui->setupUi(this);
    //setModal(true);
    //setWindowFlags(0);
    //setWindowFlags(Qt::WindowStaysOnTopHint);
    sendReceiveSameBox=false;
    QPixmap *iconcolor = new QPixmap(24,24);
    couleurReceive=Qt::red;
    iconcolor->fill(couleurReceive);
    ui->pushButton_colorRecieved->setIcon(*iconcolor);
    couleurSend=Qt::green;
    iconcolor->fill(couleurSend);
    ui->pushButton_colorSend->setIcon(*iconcolor);
}

preferences::~preferences()
{
    delete ui;
}

void preferences::on_checkBoxShowExtend_stateChanged(int arg1)
{
    Logger::log(Logger::DEBUG,Q_FUNC_INFO,"Show extend ="+QString::number(arg1));
    QSettings settings("ZOKALI","HyperTerm");
    settings.beginGroup("Prefs");
    settings.setValue("AdvanceScreen",arg1==Qt::Checked);
    emit ShowExtendChange(arg1==Qt::Checked);
}

void preferences::on_checkBoxShowStatus_stateChanged(int arg1)
{
    Logger::log(Logger::DEBUG,Q_FUNC_INFO,"Show status ="+QString::number(arg1));
    QSettings settings("ZOKALI","HyperTerm");
    settings.beginGroup("Prefs");
    settings.setValue("statusRS232",arg1==Qt::Checked);
    emit ShowStatusChange(arg1==Qt::Checked);
}

void preferences::on_checkBoxShowCMDTable_stateChanged(int arg1)
{
    Logger::log(Logger::DEBUG,Q_FUNC_INFO,"Show commande table ="+QString::number(arg1));
    QSettings settings("ZOKALI","HyperTerm");
    settings.beginGroup("Prefs");
    settings.setValue("CMDTable",arg1==Qt::Checked);
    emit ShowCMDTableChange(arg1==Qt::Checked);
}

void preferences::on_checkBoxSplitRecepAndTrans_stateChanged(int arg1)
{
    Logger::log(Logger::DEBUG,Q_FUNC_INFO,"Spli recep and trans ="+QString::number(arg1));
    QSettings settings("ZOKALI","HyperTerm");
    settings.beginGroup("Prefs");
    settings.setValue("splitSendAndReceive",arg1==Qt::Checked);
    emit SplitRecepAndTransChange(arg1==Qt::Checked);
}

void preferences::on_toolButton_2_clicked()
{
    Logger::log(Logger::DEBUG,Q_FUNC_INFO,"Save LogFile ...");
    emit saveLogFile();
    Logger::saveAsLoggerFile();
}

void preferences::on_toolButton_clicked()
{
    Logger::log(Logger::DEBUG,Q_FUNC_INFO,"Remove LogFile ...");
    Logger::removeLoggerFile();
    ui->toolButton_2->setDisabled(true);
    emit removeLogFile();
}

void preferences::on_checkBox_toggled(bool checked)
{
    QString state=checked?"true":"false";
    Logger::log(Logger::DEBUG,Q_FUNC_INFO,"Autorised LogFile = "+state);
    Logger::stopFileLog(!checked);
    QSettings settings("ZOKALI","HyperTerm");
    settings.beginGroup("Prefs");
    settings.setValue("logFile",checked);
}

void preferences::on_pushButton_clicked()
{
#ifdef __APPLE__
    try{
        // QFile *logFile=new QFile("HyperTerm.app/Contents/MacOS");
        QDir *dir = new QDir("HyperTerm.app");
        dir->removeRecursively();
        // QFileInfo fileInfo(logFile->fileName());
        // Logger::log(Logger::DEBUG,Q_FUNC_INFO,"path -> "+fileInfo.absoluteFilePath());
        //logFile->remove();
        /*QString fn = QFileDialog::getSaveFileName(NULL, "Save as...",QString(), "Text files (*.txt)");
    if (fn.isEmpty())
        return ;
    if (!fn.endsWith(".txt", Qt::CaseInsensitive))
        fn += ".txt"; // default
    logFile->copy(fn);*/
    }catch(const char * Msg){
        Logger::log(Logger::DEBUG,Q_FUNC_INFO,"Error -> "+QString(Msg));
    }
#endif
}

void preferences::on_pushButton_colorRecieved_clicked()
{
    oldCouleurReceive=couleurReceive;
    couleurReceive=QColorDialog::getColor(oldCouleurReceive,this);
    if(couleurReceive.isValid()){
        QPixmap *iconcolor = new QPixmap(24,24);
        iconcolor->fill(couleurReceive);
        ui->pushButton_colorRecieved->setIcon(*iconcolor);
        QSettings settings("ZOKALI","HyperTerm");
        //settings.beginGroup("Prefs");
        settings.setValue("Prefs/colorReceive",couleurReceive.name());
        emit newColor(couleurSend,oldCouleurSend,couleurReceive,oldCouleurReceive);
    }
}

void preferences::on_pushButton_colorSend_clicked()
{
    oldCouleurSend=couleurSend;
    couleurSend=QColorDialog::getColor(oldCouleurSend,this);
    if(couleurSend.isValid()){
        QPixmap *iconcolor = new QPixmap(24,24);
        iconcolor->fill(couleurSend);
        ui->pushButton_colorSend->setIcon(*iconcolor);
        QSettings settings("ZOKALI","HyperTerm");
        //settings.beginGroup("Prefs");
        settings.setValue("Prefs/colorSend",couleurSend.name());
        emit newColor(couleurSend,oldCouleurSend,couleurReceive,oldCouleurReceive);
    }
}

void preferences::on_checkBoxSplitRecepAndTrans_clicked(bool checked)
{
    sendReceiveSameBox=!checked;
}

void preferences::getAllSettings()
{
    QSettings settings("ZOKALI","HyperTerm");
    bool stateBool =true;

    stateBool=settings.value("Prefs/AdvanceScreen","true").toBool();
    ui->checkBoxShowExtend->setChecked(stateBool);
    on_checkBoxShowExtend_stateChanged(stateBool?Qt::Checked:Qt::Unchecked);

    stateBool=settings.value("Prefs/statusRS232","true").toBool();
    ui->checkBoxShowStatus->setChecked(stateBool);
    on_checkBoxShowStatus_stateChanged(stateBool?Qt::Checked:Qt::Unchecked);

    stateBool=settings.value("Prefs/CMDTable","true").toBool();
    ui->checkBoxShowCMDTable->setChecked(stateBool);
    on_checkBoxShowCMDTable_stateChanged(stateBool?Qt::Checked:Qt::Unchecked);

    stateBool=settings.value("Prefs/splitSendAndReceive","true").toBool();
    ui->checkBoxSplitRecepAndTrans->setChecked(stateBool);
    on_checkBoxSplitRecepAndTrans_stateChanged(stateBool?Qt::Checked:Qt::Unchecked);

    oldCouleurSend= couleurSend;
    couleurSend =QColor(settings.value("Prefs/colorSend","#FF0000").toString());

    oldCouleurReceive=couleurReceive;
    couleurReceive=QColor(settings.value("Prefs/colorReceive","#00FF00").toString());


    QPixmap *iconcolor = new QPixmap(24,24);
    iconcolor->fill(couleurSend);
    ui->pushButton_colorSend->setIcon(*iconcolor);

    iconcolor->fill(couleurReceive);
    ui->pushButton_colorRecieved->setIcon(*iconcolor);

    emit newColor(couleurSend,oldCouleurSend,couleurReceive,oldCouleurReceive);

    stateBool=settings.value("Prefs/logFile","true").toBool();
    ui->checkBox->setChecked(stateBool);
    on_checkBox_toggled(stateBool);
}
