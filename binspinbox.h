#ifndef BINSPINBOX_H
#define BINSPINBOX_H

#include <QSpinBox>

class QRegExpValidator;

class BinSpinBox : public QSpinBox
{
    Q_OBJECT
public:
    BinSpinBox(QWidget *parent = 0);
protected:
    QValidator::State validate(QString &text,int &pos) const;
    int valueFromText(const QString &text) const;
    QString textFromValue(int value) const;
private:
    QRegExpValidator *validator;

};


#endif // BINSPINBOX_H
