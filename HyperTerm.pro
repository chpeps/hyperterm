#-------------------------------------------------
#
# Project created by QtCreator 2013-04-02T21:27:47
#
#-------------------------------------------------
QT       += core gui
QT += xml
QT += network
CONFIG += static #CONFIG += staticlib

#win32:QMAKE_LFLAGS += -static-libgcc
win32:RC_FILE = hyperterm.rc

win32 {
CONFIG += embed_manifest_exe
QMAKE_LFLAGS_WINDOWS += /MANIFESTUAC:level=\'requireAdministrator\'
#QMAKE_LFLAGS += /MANIFESTUAC:\"level=\'requireAdministrator\' uiAccess=\'false\'\"
}

#ICON = ZHyperTerm.ico

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
TARGET = HyperTerm
TEMPLATE = app

#include(3rdparty/qextserialport-1.2rc/src/qextserialport.pri)
#lastest build
include(3rdparty/qextserialport/src/qextserialport.pri)

SOURCES += main.cpp\
        mainwindow.cpp \
    settingcmdwindow.cpp \
    convert.cpp \
    logger.cpp \
    uart.cpp \
    xmldom.cpp \
    processSending.cpp \
    asciichart.cpp \
    preferences.cpp \
    about.cpp \
    updaterdialog.cpp \
    updaterfen.cpp \
    hexspinbox.cpp \
    binspinbox.cpp

HEADERS  += mainwindow.h \
    settingcmdwindow.h \
    convert.h \
    logger.h \
    uart.h \
    xmldom.h \
    processSending.h \
    asciichart.h \
    preferences.h \
    about.h \
    updaterdialog.h \
    updaterfen.h \
    hexspinbox.h \
    binspinbox.h

FORMS    += mainwindow.ui \
    settingcmdwindow.ui \
    asciichart.ui \
    preferences.ui \
    about.ui

RESOURCES += \
    images.qrc

OTHER_FILES += \
    hyperterm.rc \
    HyperTerm.exe.manifest
