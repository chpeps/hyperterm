#ifndef CONVERT_H
#define CONVERT_H

#include <QString>

class convert
{
public:
    convert();
    static QString asciitoHex(QString toConvert);
    static QString asciitoHex(QString toConvert,QString prepend,QString endOfAll);
    static QString decimaltoHex(bool *ok, QString toConvert);
    static QString bintoHex(bool *ok, QString toConvert);

    static QString hextoAscii(bool *ok, QString toConvert);
    static QString decimaltoAscii(bool *ok, QString toConvert);
    static QString bintoAscii(bool *ok, QString toConvert);

    static QString hextoDecimal(bool *ok, QString toConvert);
    static QString asciitoDecimal(QString toConvert);
    static QString asciitoDecimal(QString toConvert,QString prepend,QString endOfAll);
    static QString bintoDecimal(bool *ok, QString toConvert);

    static QString asciitoBin(QString toConvert);
    static QString asciitoBin(QString toConvert,QString prepend,QString endOfAll);
    static QString hextoBin(bool *ok,QString toConvert);
    static QString decimaltoBin(bool *ok,QString toConvert);
};

#endif // CONVERT_H
