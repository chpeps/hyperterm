#ifndef LOGGER_H
#define LOGGER_H
#include <QString>
#include <QDebug>
#include <QFile>
class Logger
{

    //Logger::log(Logger::DEBUG,Q_FUNC_INFO,"");

public:
//log level
    static const int DEBUG = 0;
    static const int INFO = 1;
    static const int WARNING = 2;
    static const int CRITICAL = 3;

//out print
    static const int INTERM = 0;
    static const int INFILE = 1;
    static const int INWEB = 2;
    static const int NONE = 3;

    Logger();
    static void log(int level,QString qInfoFonction,QString message);
    static void remove();
public Q_SLOTS:
    static void removeLoggerFile();
    static void saveAsLoggerFile();
    static void stopFileLog(bool state);
private:
    static void out(QString toPrint);
    static int SELECTEDLEVELLOG;// = DEBUG;
    static int SELECTEDOUTPRINT;// = INFILE;
    static QFile *logFile;//=new QFile("logZhyperTerm.txt");

};

//QFile Logger::logFile("logZhyperTerm.txt");

#endif // LOGGER_H
