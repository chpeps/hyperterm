#ifndef PROCESS_H
#define PROCESS_H

#include <QThread>
#include <QProgressBar>
#include "qextserialport.h"
#include <QTreeWidgetItem>
#include <QList>
#include <QTreeWidget>


class Process :public QThread
{
    Q_OBJECT
public:
    const static int READALL = 0;
    const static int READCHILDREN = 1;
    const static int READONE = 2;
    const static int READNOTDEFINED = -1;
    Process(QObject* parent = NULL, QextSerialPort *port = NULL,QTreeWidget *treeWidget=NULL, int TimeIntervall=1, int howToRead=READNOTDEFINED);
    virtual ~Process();
    QProgressBar *Progression;
    QList<QTreeWidgetItem *>listTosend;
    QTreeWidget *treeWidget;
    int timeIntervall;
    int howToRead;
protected:
    virtual void run();
private:
    QextSerialPort *port;
    void processItem(QTreeWidgetItem *parent);
signals:
    void toSendText(QString CMD);
    void setMaximum(int max);
    void setValue(int value);
};

#endif // PROCESS_H
