#include "xmldom.h"
#include <QMessageBox>
#include <QFileDialog>

XmlDom::XmlDom(){

}

QString XmlDom::XmlDomGetxml(QTreeWidget *treeWidget)
{
    //QStringList list;
    //this->treeWidget=treeWidget;
    QDomDocument *dom = new QDomDocument("mon_xml");
    QString fileName = QFileDialog::getOpenFileName(NULL, "Open File","/path/to/file/","Xml Files (*.xml)");
    if(!fileName.isEmpty()){
        QFile xml_doc(fileName);// On choisit le fichier contenant les informations XML.
        if(!xml_doc.open(QIODevice::ReadOnly))// Si l'on n'arrive pas à ouvrir le fichier XML.
        {
            QMessageBox::warning(NULL,"Erreur à l'ouverture du document XML","Le document XML n'a pas pu être ouvert. Vérifiez que le nom est le bon et que le document est bien placé");
            return fileName; }
        if (!dom->setContent(&xml_doc)) // Si l'on n'arrive pas à associer le fichier XML à l'objet DOM.
        {
            xml_doc.close();
            QMessageBox::warning(NULL,"Erreur à l'ouverture du document XML","Le document XML n'a pas pu être attribué à l'objet QDomDocument.");
            return fileName;
        }
        treeWidget->clear();
        QDomElement dom_element = dom->documentElement();
        QDomNode noeud = dom_element.firstChild();

        while(!noeud.isNull())
        {
            if(!dom_element.isNull()){
                //QMessageBox::information(NULL, "Nom de la balise", "Le nom de la balise est " + dom_element.tagName());
                QDomNodeList listNode = dom_element.childNodes();
                if(dom_element.tagName().compare("TreeCmd")==0){
                    for (int i=0;i<listNode.count();i++) {
                        QDomElement item=listNode.at(i).toElement();
                        if(item.nodeName().compare("file")==0){
                            QStringList list;
                            list<<item.attribute("name","");
                            QTreeWidgetItem *itemTree = new QTreeWidgetItem(treeWidget, list);
                            itemTree->setIcon(0,QPixmap(QString::fromUtf8(":/images/images/folder.png")));
                            itemTree->setFlags(Qt::ItemIsSelectable | Qt::ItemIsUserCheckable | Qt::ItemIsEnabled | Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled | Qt::ItemIsEditable);
                            //items.append(itemTree);
                            processItem(item,itemTree);
                        }else if(item.nodeName().compare("commande")==0){
                            QStringList list;
                            list<<item.attribute("name","")<<item.attribute("CMD","")<<item.attribute("time","100")<<item.attribute("type","ASCII");
                            QTreeWidgetItem *itemTree = new QTreeWidgetItem(treeWidget, list);
                            itemTree->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsDragEnabled);
                            // items.append(itemTree);
                            // QMessageBox::information(NULL, "Nom de la balise", "Le nom de la balise est " + item.nodeName() +" item = "+item.attribute("name",""));
                        }

                    }
                }
                noeud=dom_element.nextSibling();
            }
        }

        xml_doc.close(); // Dans tous les cas, on doit fermer le document XML : on n'en a plus besoin, tout est compris dans l'objet DOM.
    }
    return fileName;
}

QString XmlDom::XmlDomSetxml(QTreeWidget *treeWidget,QString pathWhereSave)
{
    QDomDocument *dom = new QDomDocument("mon_xml");
    QString fileName =pathWhereSave;
    if(fileName.isNull() || fileName.isEmpty())
        fileName = QFileDialog::getSaveFileName(NULL, "Open File","/path/to/file/","Xml Files (*.xml)");
    if(!fileName.isEmpty()){
        QFile xml_doc(fileName);// On choisit le fichier contenant les informations XML.
        if(xml_doc.exists()){
            xml_doc.remove();
        }
        if(!xml_doc.open(QIODevice::ReadWrite))// Si l'on n'arrive pas à ouvrir le fichier XML.
        {
            QMessageBox::warning(NULL,"Erreur à l'ouverture du document XML","Le document XML n'a pas pu être ouvert. Vérifiez que le nom est le bon et que le document est bien placé");
            return fileName;
        }

        /*if (!dom->setContent(&xml_doc)) // Si l'on n'arrive pas à associer le fichier XML à l'objet DOM.
    {
        xml_doc.close();
        QMessageBox::warning(NULL,"Erreur à l'ouverture du document XML","Le document XML n'a pas pu être attribué à l'objet QDomDocument.");
        return;
    }*/
        QDomDocument doc(dom->implementation().createDocumentType("name","publicId","systemId"));
        dom->setContent(QString("<?xml version='1.0' encoding='ISO-8859-1'?>\n<TreeCmd>\n</TreeCmd>"));
        QDomElement docElem = dom->documentElement();

        for (int i = 0; i < treeWidget->topLevelItemCount(); ++i) {
            QTreeWidgetItem *item = treeWidget->topLevelItem(i);
            if(item->flags()==(Qt::ItemIsSelectable| Qt::ItemIsEnabled | Qt::ItemIsDragEnabled)){
                QDomElement write_elem = dom->createElement("commande");
                write_elem.setAttribute("name", item->data(0,Qt::DisplayRole).toString());
                write_elem.setAttribute("CMD", item->data(1,Qt::DisplayRole).toString());
                write_elem.setAttribute("time", item->data(2,Qt::DisplayRole).toString());
                write_elem.setAttribute("type", item->data(3,Qt::DisplayRole).toString());
                docElem.appendChild(write_elem);
            }else{
                QDomElement write_elem = dom->createElement("file");
                write_elem.setAttribute("name", item->data(0,Qt::DisplayRole).toString());
                setProcessItem(item,&write_elem,dom);
                docElem.appendChild(write_elem);
            }
        }

        QTextStream stream(&xml_doc);
        QString write_doc = dom->toString();
        stream << write_doc;
        xml_doc.close(); // Dans tous les cas, on doit fermer le document XML : on n'en a plus besoin, tout est compris dans l'objet DOM.
    }
    return fileName;
}

void XmlDom::setProcessItem(QTreeWidgetItem *parent,QDomElement *xmlparent,QDomDocument *dom)
{
    for (int i = 0; i < parent->childCount(); ++i) {
        QTreeWidgetItem *child = parent->child(i);
        if(child->flags()==(Qt::ItemIsSelectable| Qt::ItemIsEnabled | Qt::ItemIsDragEnabled)){
            QDomElement write_elem = dom->createElement("commande");
            write_elem.setAttribute("name", child->data(0,Qt::DisplayRole).toString());
            write_elem.setAttribute("CMD", child->data(1,Qt::DisplayRole).toString());
            write_elem.setAttribute("time", child->data(2,Qt::DisplayRole).toString());
            write_elem.setAttribute("type", child->data(3,Qt::DisplayRole).toString());
            xmlparent->appendChild(write_elem);
        }else{
            QDomElement write_elem = dom->createElement("file");
            write_elem.setAttribute("name", child->data(0,Qt::DisplayRole).toString());
            setProcessItem(child,&write_elem,dom);
            xmlparent->appendChild(write_elem);
        }
    }
}

void XmlDom::processItem(QDomElement parent,QTreeWidgetItem *treeparent)
{
    QDomNodeList listNode = parent.childNodes();
    for (int i=0;i<listNode.count();i++) {
        QDomElement item=listNode.at(i).toElement();
        if(item.nodeName().compare("file")==0){
            QStringList list;
            list<<item.attribute("name","");
            QTreeWidgetItem *itemTree = new QTreeWidgetItem(treeparent, list);
            itemTree->setIcon(0,QPixmap(QString::fromUtf8(":/images/images/folder.png")));
            itemTree->setFlags(Qt::ItemIsSelectable | Qt::ItemIsUserCheckable | Qt::ItemIsEnabled | Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled | Qt::ItemIsEditable);
            //items.append(itemTree);
            processItem(item,itemTree);
        }else if(item.nodeName().compare("commande")==0){
            QStringList list;
            list<<item.attribute("name","")<<item.attribute("CMD","")<<item.attribute("time","100")<<item.attribute("type","ASCII");
            QTreeWidgetItem *itemTree = new QTreeWidgetItem(treeparent, list);
            itemTree->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsDragEnabled);
            //  items.append(itemTree);
            // QMessageBox::information(NULL, "Nom de la balise", "Le nom de la balise est " + item.nodeName() +" item = "+item.attribute("name",""));
        }
    }
}
