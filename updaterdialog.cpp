#include "updaterdialog.h"
#include "QVBoxLayout"
#include "QMessageBox"
#include "updaterfen.h"
#include "logger.h"

updaterDialog::updaterDialog()  : QWidget()
{
    //Windows Options
    setFixedSize(300,100);
    setWindowTitle("MAJ");

    version = "0.9"; //Mettez ici la version actuel de votre application
    //C'est ce string qui sera comparer pour déterminer s'il y a bien une nouvelle version.

    label.setText("<strong>Version actuel :</strong>" + version);
    updateSoftware = new QPushButton("-- Vérifier la présence de mise à jour --", this);

    QVBoxLayout *VLayout = new QVBoxLayout(this);
    VLayout->addWidget(&label);
    VLayout->addWidget(updateSoftware);

    QObject::connect(updateSoftware,SIGNAL(clicked()),this,SLOT(updater()));
    Logger::log(Logger::DEBUG,Q_FUNC_INFO,"MAJ start");
    updater();
}

void updaterDialog::updater()
{
    Logger::log(Logger::DEBUG,Q_FUNC_INFO,"bouton clicked");
    QNetworkAccessManager manager;
    reply = manager.get(QNetworkRequest(QUrl("http://www.zokali.fr/ZhyperTerm/version.txt"))); // Url vers le fichier version.txt

    QEventLoop loop;
    QObject::connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    updateSoftware->setEnabled(false);
    loop.exec();

    versionNew = reply->readAll();
    Logger::log(Logger::DEBUG,Q_FUNC_INFO,"version distante = "+versionNew);
    updateSoftware->setEnabled(true);

    if (version != versionNew)
    {
        QMessageBox::information(this,"Logiciel non jour", "une mise à jours trouvée.");
#ifdef _WIN32
        UpdaterFen *updaterfen = new UpdaterFen();
        updaterfen->show();
        //ShellExecute(GetDesktopWindow(), (LPCWSTR)"runas", (LPCWSTR)"HyperTerm.exe", 0, 0, SW_SHOWNORMAL);
        //QProcess *qUpdater = new QProcess(this);
        //qUpdater->start("runas /noprofile /user:administrateur HyperTerm.exe");
        //close();
#elif __APPLE__
        UpdaterFen *updaterfen = new UpdaterFen();
        updaterfen->show();
#endif
    }
    else
    {
        QMessageBox::information(this,"Logiciel à jour", "Aucune mise à jours trouvée.");
    }
}
