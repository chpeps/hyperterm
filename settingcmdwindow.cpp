#include "settingcmdwindow.h"
#include "ui_settingcmdwindow.h"
#include "QDebug"
#include <QtGui>
#include "convert.h"
#include "logger.h"


settingCmdWindow::settingCmdWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::settingCmdWindow)
{
    ui->setupUi(this);
    setWindowFlags(Qt::Dialog);//SplashScreen
    connect(ui->buttonGroupType,SIGNAL(buttonClicked(int)),this,SLOT(groupButtonChange(int)));
    pastButtonValue=-2;
    QPalette palette= ui->labelName->palette();
    if(ui->lineEditName->text().compare("")==0){
        palette.setColor(QPalette::WindowText,Qt::red);

    }else{
        palette.setColor(QPalette::WindowText,Qt::white);
    }
    ui->labelName->setPalette(palette);

    if(ui->lineEditCMD->text().compare("")==0){
        palette.setColor(QPalette::WindowText,Qt::red);

    }else{
        palette.setColor(QPalette::WindowText,Qt::white);
    }
    ui->labelCMD->setPalette(palette);
    palette.setColor(QPalette::WindowText,Qt::white);
    ui->label->setPalette(palette);
}

settingCmdWindow::~settingCmdWindow()
{
    delete ui;
}

void settingCmdWindow::setName(QString name)
{
    ui->lineEditName->setText(name);
}

void settingCmdWindow::setCMD(QString CMD)
{
    ui->lineEditCMD->setText(CMD);
}

void settingCmdWindow::setType(QString type)
{
    if(type.compare("ASCII")==0){
        ui->radioButtonAscii->setChecked(true);
        pastButtonValue=ASCII;
    }else if(type.compare("HEX")==0){
        ui->radioButtonHex->setChecked(true);
        pastButtonValue=HEX;
    }else if(type.compare("DECIMAL")==0){
        ui->radioButtonDecimal->setChecked(true);
        pastButtonValue=DECIMAL;
    }else if(type.compare("BIN")==0){
        ui->radioButtonBin->setChecked(true);
        pastButtonValue=BIN;
    }
}

void settingCmdWindow::setItemToEdit(QTreeWidgetItem *itemEdited)
{
    QString cmd=itemEdited->data(1,Qt::DisplayRole).toString();
    ui->lineEditName->setText(itemEdited->data(0,Qt::DisplayRole).toString());
    ui->spinBox->setValue(itemEdited->data(2,Qt::DisplayRole).toInt());
    setType(itemEdited->data(3,Qt::DisplayRole).toString());
    this->itemEdited=itemEdited;

    switch(pastButtonValue){
    case ASCII:
        break;
    case HEX:
        cmd=convert::asciitoHex(cmd);
        break;
    case DECIMAL:
        cmd=convert::asciitoDecimal(cmd);
        break;
    case BIN:
        cmd=convert::asciitoBin(cmd);
        break;
    }
    isEditMode=true;
    ui->lineEditCMD->setText(cmd);
}

void settingCmdWindow::on_pushButtonAdd_clicked()
{
    Logger::log(Logger::INFO,Q_FUNC_INFO,"button Add(+) clicked");
    if(ui->lineEditName->text().compare("")==0){
        Logger::log(Logger::DEBUG,Q_FUNC_INFO,"lineEditName gain focus");
        ui->lineEditName->setFocus();
    }

    if(ui->lineEditCMD->text().compare("")==0){
        Logger::log(Logger::DEBUG,Q_FUNC_INFO,"lineEditCMD gain focus");
        ui->lineEditCMD->setFocus();
    }

    if(!(ui->lineEditName->text().compare("")==0 || ui->lineEditCMD->text().compare("")==0)){
        QString type="";
        bool ok=false;
        QString cmd=ui->lineEditCMD->text();
        Logger::log(Logger::DEBUG,Q_FUNC_INFO,"Tous les champs sont remplit,signal dataAdded emit (pastButtonValue)="+QString::number(pastButtonValue));

        switch(pastButtonValue){
        case ASCII:
            type="ASCII";
            ok=true;
            break;
        case HEX:
            type="HEX";
            cmd=convert::hextoAscii(&ok,cmd);
            break;
        case DECIMAL:
            type="DECIMAL";
            cmd=convert::decimaltoAscii(&ok,cmd);
            break;
        case BIN:
            type="BIN";
            cmd=convert::bintoAscii(&ok,cmd);
            break;
        }

        if(ok){

            if(itemEdited!=NULL){
                itemEdited->setData(0,Qt::DisplayRole,ui->lineEditName->text());
                itemEdited->setData(1,Qt::DisplayRole,cmd);
                itemEdited->setData(2,Qt::DisplayRole,ui->spinBox->value());
                itemEdited->setData(3,Qt::DisplayRole,type);
                itemEdited->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsDragEnabled);
                if(!isEditMode)
                    emit dataAdded(itemEdited);
            }else{

                if(!isEditMode){
                    emit dataAdded(ui->lineEditName->text(),ui->lineEditCMD->text(),type,QString::number(ui->spinBox->value()));
                }else{
                    emit dataEdited(ui->lineEditName->text(),ui->lineEditCMD->text(),type,editedRow);
                }
            }
            typeCMD=type;
            name=ui->lineEditName->text();
            CMD=ui->lineEditCMD->text();

            ui->lineEditCMD->clear();
            ui->lineEditName->setFocus();
            ui->lineEditName->clear();
            ui->radioButtonAscii->setChecked(true);
            pastButtonValue=ASCII;
            hide();
        }else{
            QPalette palette;
            palette.setColor(QPalette::WindowText,Qt::red);
            ui->labelCMD->setPalette(palette);
        }
    }
}

void settingCmdWindow::groupButtonChange(int button)
{
    if(pastButtonValue!=button && ui->lineEditCMD->text().compare("")!=0){
        Logger::log(Logger::INFO,Q_FUNC_INFO,"GroupButton value change from "+QString::number(pastButtonValue,10)+" to "+QString::number(button,10));
        QString toConvert=ui->lineEditCMD->text();
        QString toReturn="";
        QPalette paletteLineCMD = ui->lineEditCMD->palette();
        bool isOk=true;
        switch(button){
        case ASCII:
            switch(pastButtonValue){
            case HEX:
                toReturn=convert::hextoAscii(&isOk,toConvert);
                Logger::log(Logger::DEBUG,Q_FUNC_INFO,"HEX to ASCII from \""+toConvert+"\" to \""+toReturn+"\"  convertion "+(isOk?"true":"false"));
                break;
            case DECIMAL:
                toReturn=convert::decimaltoAscii(&isOk,toConvert);
                Logger::log(Logger::DEBUG,Q_FUNC_INFO,"DECIMAL to ASCII from \""+toConvert+"\" to \""+toReturn+"\"  convertion "+(isOk?"true":"false"));
                break;
            case BIN:
                toReturn=convert::bintoAscii(&isOk,toConvert);
                Logger::log(Logger::DEBUG,Q_FUNC_INFO,"BIN to ASCII from \""+toConvert+"\" to \""+toReturn+"\" convertion "+(isOk?"true":"false"));
                break;
            }
            break;
        case HEX:
            switch(pastButtonValue){
            case ASCII:
                toReturn=convert::asciitoHex(toConvert);
                Logger::log(Logger::DEBUG,Q_FUNC_INFO,"ASCII to HEX from \""+toConvert+"\" to \""+toReturn+"\"");
                break;
            case DECIMAL:
                toReturn=convert::decimaltoHex(&isOk,toConvert);
                Logger::log(Logger::DEBUG,Q_FUNC_INFO,"DECIMAL to HEX from \""+toConvert+"\" to \""+toReturn+"\"  convertion "+(isOk?"true":"false"));
                break;
            case BIN:
                toReturn=convert::bintoHex(&isOk,toConvert);
                Logger::log(Logger::DEBUG,Q_FUNC_INFO,"BIN to HEX from \""+toConvert+"\" to \""+toReturn+"\" convertion "+(isOk?"true":"false"));
                break;
            }
            break;
        case DECIMAL:
            switch(pastButtonValue){
            case ASCII:
                toReturn=convert::asciitoDecimal(toConvert);
                Logger::log(Logger::DEBUG,Q_FUNC_INFO,"ASCII to DECIMAL from \""+toConvert+"\" to \""+toReturn+"\"");
                break;
            case HEX:
                toReturn=convert::hextoDecimal(&isOk,toConvert);
                Logger::log(Logger::DEBUG,Q_FUNC_INFO,"HEX to DECIMAL from \""+toConvert+"\" to \""+toReturn+"\" convertion "+(isOk?"true":"false"));
                break;
            case BIN:
                toReturn=convert::bintoDecimal(&isOk,toConvert);
                Logger::log(Logger::DEBUG,Q_FUNC_INFO,"BIN to DECIMAL from \""+toConvert+"\" to \""+toReturn+"\" convertion "+(isOk?"true":"false"));
                break;
            }
            break;
        case BIN:
            switch(pastButtonValue){
            case ASCII:
                toReturn=convert::asciitoBin(toConvert);
                Logger::log(Logger::DEBUG,Q_FUNC_INFO,"ASCII to BIN from \""+toConvert+"\" to \""+toReturn+"\"");
                break;
            case HEX:
                toReturn=convert::hextoBin(&isOk,toConvert);
                Logger::log(Logger::DEBUG,Q_FUNC_INFO,"HEX to BIN from \""+toConvert+"\" to \""+toReturn+"\" convertion "+(isOk?"true":"false"));
                break;
            case DECIMAL:
                toReturn=convert::decimaltoBin(&isOk,toConvert);
                Logger::log(Logger::DEBUG,Q_FUNC_INFO,"DECIMAL to BIN from \""+toConvert+"\" to \""+toReturn+"\" convertion "+(isOk?"true":"false"));
                break;
            }
            break;
            break;
        }
        if(isOk){
            paletteLineCMD.setColor(QPalette::Text,Qt::black);
            ui->lineEditCMD->clear();
            ui->lineEditCMD->setText(toReturn);
            pastButtonValue=button;
        }else{
            if(ui->lineEditCMD->text().compare("")==0){
                pastButtonValue=button;
            }else{
                paletteLineCMD.setColor(QPalette::Text,Qt::red);
                switch(pastButtonValue){
                case ASCII:
                    ui->radioButtonAscii->setChecked(true);
                    break;
                case HEX:
                    ui->radioButtonHex->setChecked(true);
                    break;
                case DECIMAL:
                    ui->radioButtonDecimal->setChecked(true);
                    break;
                }
            }

        }
        ui->lineEditCMD->setPalette(paletteLineCMD);
    }
}

void settingCmdWindow::keyPressEvent(QKeyEvent* keyEvent)
{
    if (keyEvent->key() == Qt::Key_Return &&
            keyEvent->modifiers() == Qt::ControlModifier)
    {
        Logger::log(Logger::DEBUG,Q_FUNC_INFO,"ctrl+enter event detected add \\n at the cursor position");
        //ui->lineEditCMD->setText( ui->lineEditCMD->text().append("\n"));
        int position = ui->lineEditCMD->cursorPosition();
        QString toPrint="";
        switch(pastButtonValue){
        case ASCII:
            toPrint="\n";
            break;
        case HEX:
            toPrint=QString::number('\n',16).toUpper();
            break;
        case DECIMAL:
            toPrint=QString::number('\n',10);
            break;
        }
        if(pastButtonValue==HEX || pastButtonValue==DECIMAL){
            if(position==0){
                toPrint.append("|");
            }else if(position==ui->lineEditCMD->text().size()){
                toPrint.prepend("|");
            }else if(ui->lineEditCMD->text().at(position) == '|'){
                toPrint.prepend("|");
            }else if(ui->lineEditCMD->text().at(position-1) == '|'){
                toPrint.append("|");
            }else{
                toPrint.prepend("|");
                toPrint.append("|");
            }
        }
        ui->lineEditCMD->insert(toPrint);

    }
}

void settingCmdWindow::on_lineEditCMD_textChanged(const QString &arg1)
{
    QPalette paletteLineCMD = ui->lineEditCMD->palette();
    paletteLineCMD.setColor(QPalette::Text,Qt::black);
    QPalette palette= ui->labelCMD->palette();
    if(arg1.compare("")==0){
        palette.setColor(QPalette::WindowText,Qt::red);

    }else{
        palette.setColor(QPalette::WindowText,Qt::white);
    }
    ui->labelCMD->setPalette(palette);
    ui->lineEditCMD->setPalette(paletteLineCMD);
}

void settingCmdWindow::on_lineEditName_textChanged(const QString &arg1)
{
    QPalette palette= ui->labelName->palette();
    if(arg1.compare("")==0){
        palette.setColor(QPalette::WindowText,Qt::red);

    }else{
        palette.setColor(QPalette::WindowText,Qt::white);
    }
    ui->labelName->setPalette(palette);
}

void settingCmdWindow::on_lineEditCMD_returnPressed()
{
    ui->pushButtonAdd->click();
}
