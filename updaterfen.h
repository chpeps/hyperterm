#ifndef UPDATERFEN_H
#define UPDATERFEN_H

#include <QApplication>
#include <QtGui>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>
#include <QCoreApplication>
#include <QUrl>
#include <QProgressBar>

class UpdaterFen : public QWidget
{
   Q_OBJECT

   public:
       UpdaterFen();

   private:
       QProgressBar *progression;
       QNetworkReply *reply;
       QNetworkAccessManager manager;

   public slots:
       void progressionTelechargement(qint64 bytesReceived, qint64 bytesTotal);
       void enregistrer();

};

#endif // UPDATERFEN_H
