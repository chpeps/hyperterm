#ifndef PREFERENCES_H
#define PREFERENCES_H

#include <QDialog>

namespace Ui {
class preferences;
}

class preferences : public QDialog
{
    Q_OBJECT
    
public:
    explicit preferences(QWidget *parent = 0);
    ~preferences();
    void getAllSettings();
    QColor couleurSend;
    QColor couleurReceive;
    QColor oldCouleurSend;
    QColor oldCouleurReceive;
    bool sendReceiveSameBox;
signals:
    void ShowStatusChange(bool arg1);
    void ShowExtendChange(bool arg1);
    void ShowCMDTableChange(bool arg1);
    void SplitRecepAndTransChange(bool arg1);
    void saveLogFile();
    void removeLogFile();
    void newColor(QColor newcolorSend,QColor oldcolorSend,QColor newColorReceived,QColor oldcolorReceived);
    
private slots:
    void on_checkBoxShowExtend_stateChanged(int arg1);

    void on_checkBoxShowStatus_stateChanged(int arg1);

    void on_checkBoxShowCMDTable_stateChanged(int arg1);

    void on_checkBoxSplitRecepAndTrans_stateChanged(int arg1);

    void on_toolButton_2_clicked();

    void on_toolButton_clicked();

    void on_checkBox_toggled(bool checked);

    void on_pushButton_clicked();

    void on_pushButton_colorRecieved_clicked();

    void on_pushButton_colorSend_clicked();

    void on_checkBoxSplitRecepAndTrans_clicked(bool checked);

private:
    Ui::preferences *ui;
};

#endif // PREFERENCES_H
